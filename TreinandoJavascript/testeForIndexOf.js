let teste = "cavalo branco grande";
let teste2 = "cavalo preto grande";
let teste3 = "gato preto magro";
let teste4 = "gato branco verde";

let lista = [];
let lista2 = [];
lista.push(teste,teste2,teste3,teste4);

let branco = "branco";

for (let i = 0; i < lista.length; i++) {
      if(lista[i].indexOf(branco) > 0){
        lista2.push(lista[i]);
      }  
}

function filtrando (palavra){
  return lista.filter( linha => linha.indexOf(palavra) >= 0 );
}

console.log(filtrando(branco));

// ------------------MAP--------------------------------

// https://www.youtube.com/watch?v=nYRIRZBHQ3s

const array = [
  {nome: '...', preco: 23.50, desconto: 0.2},
  {nome: '...', preco: 120.99, desconto: 0.3},
  {nome: '...', preco: 50.50, desconto: 0.1},
  {nome: '...', preco: 40.50, desconto: 0.5},
  {nome: '...', preco: 10.50, desconto: 0.3},
  {nome: '...', preco: 11.50, desconto: 0.2}
];

function aplicarDesconto(produto) {
  return produto.preco * (1 - produto.desconto);
}

const resultado = array.map(aplicarDesconto);

// -------------------------FILTER-----------------------------

const notas = [7.1, 8.3, 6.3, 7.7, 9.1, 4.3];

function aprovado(nota){
  return nota >= 7;
}

const alunosAprovados = notas.filter(aprovado);
// console.log(alunosAprovados);