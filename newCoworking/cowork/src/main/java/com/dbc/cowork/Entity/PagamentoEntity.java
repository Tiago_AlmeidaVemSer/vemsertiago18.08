package com.dbc.cowork.Entity;

import com.dbc.cowork.Enum.TipoPagamentoEnum;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "pagamento")
public class PagamentoEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE_PACOTE")
    private ClientePacoteEntity clientePacote;

    @ManyToOne
    @JoinColumn(name = "ID_CONTRATACAO")
    private ContratacaoEntity contratacao;

    @Enumerated(EnumType.STRING)
    private TipoPagamentoEnum tipoPagamento;

    public PagamentoEntity() {
    }

    public PagamentoEntity(ClientePacoteEntity clientePacote, ContratacaoEntity contratacao, TipoPagamentoEnum tipoPagamento) {
        this.clientePacote = clientePacote;
        this.contratacao = contratacao;
        this.tipoPagamento = tipoPagamento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientePacoteEntity getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacoteEntity clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PagamentoEntity)) return false;
        PagamentoEntity that = (PagamentoEntity) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
