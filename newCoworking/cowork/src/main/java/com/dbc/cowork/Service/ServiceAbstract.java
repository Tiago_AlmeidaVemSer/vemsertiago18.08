package com.dbc.cowork.Service;

import com.dbc.cowork.Entity.EntityAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public abstract class ServiceAbstract<
        R extends JpaRepository<E, T>,
        E extends EntityAbstract, T> {

    @Autowired
    protected R repository;

    @Transactional( rollbackFor = Exception.class )
    public E save( E entidade ) {
        return repository.save(entidade);
    }

    @Transactional( rollbackFor = Exception.class )
    public E update( E entidade, T id ) {
        entidade.setId(id);
        return repository.save(entidade);
    }

    public List<E> findAll() {
        return (List<E>) repository.findAll();
    }

    public E findById( T id ) {
        return repository.findById(id).get();
    }
}
