package com.dbc.cowork.Entity;

import com.dbc.cowork.Enum.TipoContratacaoEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "contratacao")
public class ContratacaoEntity extends EntityAbstract<Integer> implements Serializable {

    @Id
    @SequenceGenerator(name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private TipoContratacaoEnum tipoContratacaoEnum;
    private int quantidade; // equivale a quantidade do tipo de contratacao
    private int desconto = 0; //opcional
    private int prazo; // numero em dias

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE", nullable = false)
    private ClienteEntity cliente;

    @ManyToOne
    @JoinColumn(name = "ID_ESPACO", nullable = false)
    private EspacoEntity espaco;

    @OneToMany(mappedBy = "contratacao")
    private List<PagamentoEntity> pagamentos;

    public ContratacaoEntity(){}

    public ContratacaoEntity(TipoContratacaoEnum tipoContratacaoEnum, int quantidade, int desconto, int prazo, ClienteEntity cliente, EspacoEntity espaco) {
        this.tipoContratacaoEnum = tipoContratacaoEnum;
        this.quantidade = quantidade;
        this.desconto = desconto;
        this.prazo = prazo;
        this.cliente = cliente;
        this.espaco = espaco;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacaoEnum() {
        return tipoContratacaoEnum;
    }

    public void setTipoContratacaoEnum(TipoContratacaoEnum tipoContratacaoEnum) {
        this.tipoContratacaoEnum = tipoContratacaoEnum;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getDesconto() {
        return desconto;
    }

    public void setDesconto(int desconto) {
        this.desconto = desconto;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public double getTotalContratacao(){
        double totalSemDesconto = this.espaco.getValor() * this.quantidade;
        if(desconto > 0){
            return totalSemDesconto - (((totalSemDesconto) * desconto) / 100);
        }else{
            return totalSemDesconto;
        }
    }

    public List<PagamentoEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentoEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContratacaoEntity)) return false;
        ContratacaoEntity that = (ContratacaoEntity) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
