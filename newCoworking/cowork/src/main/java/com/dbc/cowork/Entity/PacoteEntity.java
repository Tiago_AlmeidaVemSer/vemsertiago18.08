package com.dbc.cowork.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "pacote")
public class PacoteEntity extends EntityAbstract<Integer> implements Serializable {

    @Id
    @SequenceGenerator(name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private double valor;

    @OneToMany(mappedBy = "pacote")
    private List<EspacoPacoteEntity> espacoPacotes;

    @OneToMany(mappedBy = "pacote")
    private List<ClientePacoteEntity> clientePacotes;

    public PacoteEntity() {
    }

    public PacoteEntity(double valor) {
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<ClientePacoteEntity> getClientePacotes() {
        return clientePacotes;
    }

    public void setClientePacotes(List<ClientePacoteEntity> clientePacotes) {
        this.clientePacotes = clientePacotes;
    }

    public List<EspacoPacoteEntity> getEspacoPacotes() {
        return espacoPacotes;
    }

    public void setEspacoPacotes(List<EspacoPacoteEntity> espacoPacotes) {
        this.espacoPacotes = espacoPacotes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PacoteEntity)) return false;
        PacoteEntity that = (PacoteEntity) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
