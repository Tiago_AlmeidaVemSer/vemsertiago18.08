package com.dbc.cowork.Repository;

import com.dbc.cowork.Entity.ClienteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<ClienteEntity, Integer> {
    ClienteEntity findByNome(String nome);
    ClienteEntity findByCpf(String cpf);
}
