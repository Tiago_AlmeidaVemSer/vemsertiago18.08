package com.dbc.cowork.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "contato")
public class ContatoEntity extends EntityAbstract<Integer> implements Serializable {

    @Id
    @SequenceGenerator(name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String valor;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn(name = "ID_TIPO_CONTATO")
    private TipoContatoEntity tipoContato;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_CLIENTE")
    private ClienteEntity cliente;

    public ContatoEntity() {
    }

    public ContatoEntity(String valor) {
        this.valor = valor;
    }

    public ContatoEntity(TipoContatoEntity tipoContato, ClienteEntity cliente,
                         String valor) {
        this.tipoContato = tipoContato;
        this.cliente = cliente;
        this.valor = valor;
    }

    public ContatoEntity(Integer id, TipoContatoEntity tipoContato,
                         ClienteEntity cliente, String valor) {
        this.id = id;
        this.tipoContato = tipoContato;
        this.cliente = cliente;
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContatoEntity)) return false;
        ContatoEntity that = (ContatoEntity) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
