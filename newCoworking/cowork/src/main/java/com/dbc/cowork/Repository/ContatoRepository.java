package com.dbc.cowork.Repository;

import com.dbc.cowork.Entity.ClienteEntity;
import com.dbc.cowork.Entity.ContatoEntity;
import com.dbc.cowork.Entity.TipoContatoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContatoRepository extends JpaRepository<ContatoEntity, Integer> {
    public ContatoEntity findByValor(String valor);
    List<ContatoEntity> findAllByTipoContato(TipoContatoEntity tipoContato);
    List<ContatoEntity> findAllByCliente(ClienteEntity cliente);
}
