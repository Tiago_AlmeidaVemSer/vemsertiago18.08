package com.dbc.cowork.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "cliente")
public class ClienteEntity extends EntityAbstract<Integer> implements Serializable {

    @Id
    @SequenceGenerator(name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false)
    private String nome;

    @Column(length = 11, nullable = false, unique = true)
    private String cpf;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate dataNascimento;

    @OneToMany( mappedBy = "cliente")
    private List<ContatoEntity> contatos;

    @OneToMany(mappedBy = "cliente")
    private List<ContratacaoEntity> contratacoes;

    @OneToMany(mappedBy = "cliente")
    private List<ClientePacoteEntity> clientePacotes;

    public ClienteEntity(){}

    public ClienteEntity(String nome, String cpf, LocalDate dataNascimento) {
        this.nome = nome;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }

    public List<ContratacaoEntity> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<ContratacaoEntity> contratacoes) {
        this.contratacoes = contratacoes;
    }

    public List<ClientePacoteEntity> getClientePacotes() {
        return clientePacotes;
    }

    public void setClientePacotes(List<ClientePacoteEntity> clientePacotes) {
        this.clientePacotes = clientePacotes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClienteEntity)) return false;
        ClienteEntity that = (ClienteEntity) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
