package com.dbc.cowork.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "tipo_contato")
public class TipoContatoEntity extends EntityAbstract<Integer> implements Serializable {

    @Id
    @SequenceGenerator(name = "TIPO_CONTATO_SEQ", sequenceName = "TIPO_CONTATO_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "TIPO_CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String nome;

    public TipoContatoEntity(){
    }

    public TipoContatoEntity(String nome) {
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TipoContatoEntity)) return false;
        TipoContatoEntity that = (TipoContatoEntity) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
