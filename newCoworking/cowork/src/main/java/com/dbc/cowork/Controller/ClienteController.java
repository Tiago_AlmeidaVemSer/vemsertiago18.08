package com.dbc.cowork.Controller;

import com.dbc.cowork.Entity.ClienteEntity;
import com.dbc.cowork.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping(value = "/novo")
    @ResponseBody
    public ClienteEntity novo(@RequestBody ClienteEntity cliente) {

        return clienteService.save(cliente);
    }

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ClienteEntity> todos(){
        return clienteService.findAll();
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ClienteEntity porId(@PathVariable int id){
        return clienteService.findById(id);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ClienteEntity editar(@RequestBody ClienteEntity clienteEntity, @PathVariable Integer id){
        return clienteService.update(clienteEntity,id);
    }

}
