package com.dbc.cowork.Service;

import com.dbc.cowork.DTO.ContatoDTO;
import com.dbc.cowork.Entity.ContatoEntity;
import com.dbc.cowork.Repository.ClienteRepository;
import com.dbc.cowork.Repository.ContatoRepository;
import com.dbc.cowork.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContatoService extends ServiceAbstract<ContatoRepository, ContatoEntity, Integer>{

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Transactional(rollbackFor = Exception.class)
    public ContatoEntity novo(ContatoDTO contatoDTO) {

        return repository.save(
                new ContatoEntity(
                    tipoContatoRepository.findByNome(contatoDTO.getTipoContato()),
                    clienteRepository.findByCpf(contatoDTO.getCpfCliente()),
                    contatoDTO.getValorContato()
            ));

    }

    @Transactional(rollbackFor = Exception.class)
    public ContatoEntity editar(ContatoDTO contatoDTO, int id) {

        return repository.save(
                new ContatoEntity(
                        id,
                        tipoContatoRepository.findByNome(contatoDTO.getTipoContato()),
                        clienteRepository.findByCpf(contatoDTO.getCpfCliente()),
                        contatoDTO.getValorContato()
                )
        );
    }

    public List<ContatoEntity> todosPorTipoContato(String tipo) {

        return repository.findAllByTipoContato(
                tipoContatoRepository.findByNome(tipo)
        );
    }

}
