package com.dbc.cowork.Controller;

import com.dbc.cowork.Entity.TipoContatoEntity;
import com.dbc.cowork.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/tipocontato")
public class TipoContatoController {

    @Autowired
    private TipoContatoService service;

    @PostMapping("/novo")
    @ResponseBody
    public TipoContatoEntity novoTipoContato (@RequestBody TipoContatoEntity tipoContato){
        return service.save(tipoContato);
    }

   @GetMapping(value = "/{id}")
   @ResponseBody
    public ResponseEntity<TipoContatoEntity> findById(@PathVariable Integer id){
       TipoContatoEntity tipoContato = service.findById(id);
       return ResponseEntity.ok().body(tipoContato);
    }

    @GetMapping(value = "/nome/{nome}")
    @ResponseBody
    public TipoContatoEntity findByNome(@PathVariable String nome){
        return Optional.ofNullable(service.findByNome(nome)).get();
    }

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<TipoContatoEntity> todos(){
        return service.findAll();
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public TipoContatoEntity editar(@RequestBody TipoContatoEntity tipoContato, @PathVariable Integer id){
        return service.update(tipoContato, id);
    }

}
