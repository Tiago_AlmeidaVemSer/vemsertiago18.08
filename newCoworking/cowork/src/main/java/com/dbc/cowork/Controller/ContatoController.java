package com.dbc.cowork.Controller;

import com.dbc.cowork.DTO.ContatoDTO;
import com.dbc.cowork.Entity.ContatoEntity;
import com.dbc.cowork.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("contato")
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @GetMapping(value = "novo")
    @ResponseBody
    public ContatoEntity novo(@RequestBody ContatoDTO contatoDTO){
        return contatoService.novo(contatoDTO);
    }

    @GetMapping(value = "{id}")
    @ResponseBody
    public ContatoEntity porId(@PathVariable int id) {
        return Optional.ofNullable(
                contatoService.findById(id)
        ).get();
    }

    @GetMapping(value = "todos")
    @ResponseBody
    public List<ContatoEntity> todos(){
        return contatoService.findAll();
    }

    @GetMapping(value = "editar/{id}")
    @ResponseBody
    public ContatoEntity editar(@RequestBody ContatoDTO contatoDTO, @PathVariable int id){
        return contatoService.editar(contatoDTO, id);
    }

}
