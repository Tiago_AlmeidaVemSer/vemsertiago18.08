package com.dbc.cowork.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class AcessoEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumns({
            @JoinColumn(name = "ID_CLIENTE", nullable = false),
            @JoinColumn(name = "ID_ESPACO", nullable = false)
    })
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private SaldoClienteEntity saldoCliente;

    private boolean isEntrada;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private LocalDateTime dataDoAcesso;
    private Boolean isExcecao;

    {
        this.isEntrada = false;
        this.isExcecao = false;
    }

    public AcessoEntity() {
    }

    public AcessoEntity(SaldoClienteEntity saldoCliente,
                        boolean isEntrada, LocalDateTime dataDoAcesso, boolean isExcecao) {
        this.saldoCliente = saldoCliente;
        this.isEntrada = isEntrada;
        this.dataDoAcesso = dataDoAcesso;
        this.isExcecao = isExcecao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public LocalDateTime getDataDoAcesso() {
        return dataDoAcesso;
    }

    public void setDataDoAcesso(LocalDateTime dataDoAcesso) {
        boolean verificaData = dataDoAcesso == null;
        if(verificaData) {
            dataDoAcesso = LocalDateTime.now();
        }else{
            this.dataDoAcesso = dataDoAcesso;
        }
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }
}
