package com.dbc.cowork.Repository;

import com.dbc.cowork.Entity.TipoContatoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoContatoRepository extends JpaRepository<TipoContatoEntity, Integer> {
    TipoContatoEntity findByNome(String nome);
}
