package com.dbc.cowork.Entity;

import com.dbc.cowork.Enum.TipoContratacaoEnum;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "espaco_pacote")
public class EspacoPacoteEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "ESPACO_PACOTE_SEQ", sequenceName = "ESPACO_PACOTE_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "ESPACO_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private  Integer id;

    @ManyToOne
    @JoinColumn(name = "ID_ESPACO")
    private EspacoEntity espaco;

    @ManyToOne
    @JoinColumn(name = "ID_PACOTE")
    private PacoteEntity pacote;

    @Enumerated(EnumType.STRING)
    private TipoContratacaoEnum tipoContratacao;

    private int quantidade;
    private int prazo;

    public EspacoPacoteEntity() {
    }

    public EspacoPacoteEntity(EspacoEntity espaco, PacoteEntity pacote, TipoContratacaoEnum tipoContratacao, int quantidade, int prazo) {
        this.espaco = espaco;
        this.pacote = pacote;
        this.tipoContratacao = tipoContratacao;
        this.quantidade = quantidade;
        this.prazo = prazo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EspacoPacoteEntity)) return false;
        EspacoPacoteEntity that = (EspacoPacoteEntity) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
