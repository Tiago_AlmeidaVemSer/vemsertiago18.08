package com.dbc.cowork.Repository;

import com.dbc.cowork.Entity.TipoContatoEntity;
import com.dbc.cowork.Entity.UsuarioEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<UsuarioEntity, Integer> {
    UsuarioEntity findByNome(String nome);
}
