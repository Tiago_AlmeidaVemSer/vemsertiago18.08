package com.dbc.cowork.Service;

import com.dbc.cowork.Entity.TipoContatoEntity;
import com.dbc.cowork.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TipoContatoService {

    @Autowired
    private TipoContatoRepository repository;

    public TipoContatoEntity findById(Integer id){
        TipoContatoEntity tipoContato = repository.findById(id).get();
        return tipoContato;
    }

    @Transactional( rollbackFor = Exception.class)
    public TipoContatoEntity save(TipoContatoEntity tipoContato){
        return repository.save(tipoContato);
    }

    @Transactional( rollbackFor = Exception.class)
    public List<TipoContatoEntity> findAll(){
        return repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class)
    public TipoContatoEntity update(TipoContatoEntity tipoContato,Integer id){
        tipoContato.setId(id);
        return repository.save(tipoContato);
    }

    @Transactional( rollbackFor = Exception.class)
    public TipoContatoEntity findByNome(String nome){
        return repository.findByNome(nome);
    }
}
