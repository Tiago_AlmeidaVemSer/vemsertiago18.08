package com.dbc.cowork.Service;

import com.dbc.cowork.Entity.UsuarioEntity;
import com.dbc.cowork.Repository.UsuarioRepository;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService extends ServiceAbstract<UsuarioRepository,UsuarioEntity,Integer>{

    public UsuarioEntity findByNome(String nome){
        return repository.findByNome(nome);
    }

}
