package com.dbc.cowork.Controller;

import com.dbc.cowork.Entity.UsuarioEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/usuario")
public class UsuarioController {

    @GetMapping
    public ResponseEntity<UsuarioEntity> findAll(){
        UsuarioEntity usuario = new UsuarioEntity("maria", "maria.com", "mariapiriguete123", "123456");
        return ResponseEntity.ok().body(usuario);
    }

}
