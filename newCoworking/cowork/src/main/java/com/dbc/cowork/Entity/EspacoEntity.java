package com.dbc.cowork.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "espaco")
public class EspacoEntity extends EntityAbstract<Integer> implements Serializable {

    @Id
    @SequenceGenerator(name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String nome;

    @Column(nullable = false)
    private int qtdPessoas;

    @Column(nullable = false)
    private double valor;

    @OneToMany(mappedBy = "espaco")
    private List<ContratacaoEntity> contratacoes;

    @OneToMany(mappedBy = "espaco")
    private List<EspacoPacoteEntity> espacoPacotes;

    public EspacoEntity(){

    }

    public EspacoEntity(String nome, int qtdPessoas, double valor) {
        this.nome = nome;
        this.qtdPessoas = qtdPessoas;
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<ContratacaoEntity> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<ContratacaoEntity> contratacoes) {
        this.contratacoes = contratacoes;
    }

    public List<EspacoPacoteEntity> getEspacoPacotes() {
        return espacoPacotes;
    }

    public void setEspacoPacotes(List<EspacoPacoteEntity> espacoPacotes) {
        this.espacoPacotes = espacoPacotes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EspacoEntity)) return false;
        EspacoEntity that = (EspacoEntity) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
