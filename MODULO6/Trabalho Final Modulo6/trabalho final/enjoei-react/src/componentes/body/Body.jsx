import React from 'react'
import Card from './index'
import './Card.css'

export default (props) => {
    return (
        <React.Fragment className="Card">
            <div className="titulo">
                <h2>Vestidos</h2>
                <p>chic chic chic</p>
            </div>
            <div>
                <div>
                    <Card />
                    <Card />
                    <Card />
                    <Card />
                </div>
                <div>
                    <Card />
                    <Card />
                    <Card />
                    <Card />
                </div>
            </div>
        </React.Fragment>
    )
}