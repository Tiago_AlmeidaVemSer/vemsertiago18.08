import React from 'react';
import { Link } from 'react-router-dom';

const CardProduto = ({ produto }) =>
      <React.Fragment>
          <div className='col col-sm-3 container-card-reponsivo-colecao'>
            <Link to={{ pathname: `/interno`, state: { produto: produto } }}>
              <img className='row container-card-item-colecao' src={ produto.imagem } alt='banner' />
              <div className='preco-produto-card' > { `R$ ${produto.preco}` } </div>
            </Link>
          </div>
      </React.Fragment>

export default CardProduto;