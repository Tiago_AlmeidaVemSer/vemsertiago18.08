import React, { Component } from 'react';
import Carousel from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import CarUm from "../../imagens/car-1.gif";
import CarDois from "../../imagens/car-2.png";
import CarTres from "../../imagens/car-3.jpg";
import CarQuatro from "../../imagens/car-4.png";
import CarCinco from "../../imagens/car-5.jpg";

export default class MyCarousel extends Component {
  render() {
    return (
      <Carousel autoPlay={3000} infinite>
        <img src={CarUm} />
        <img src={CarDois} />
        <img src={CarTres} />
        <img src={CarQuatro}/>
        <img src={CarCinco}/>
      </Carousel>
    );
  }
}