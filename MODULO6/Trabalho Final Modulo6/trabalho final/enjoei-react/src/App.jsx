import Banner from './componentes/banner'
import Body from './componentes/body/Body'
import Card from './componentes/card'
import Header from './componentes/header'
import HeaderInicial from './componentes/headerInicial'
import React from 'react'
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <HeaderInicial />
      <Header />
      <Banner />
      <Body />
      <Card />
    </div>
  )
}

export default App;
