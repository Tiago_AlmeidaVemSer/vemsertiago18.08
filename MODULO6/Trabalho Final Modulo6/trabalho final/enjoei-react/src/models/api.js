import axios from 'axios';

const url = 'http://localhost:9000/api/';

const _get = url => new Promise( ( resolve, reject ) => axios.get( url ).then( response => resolve( response.data ) ) );
const _post = ( url, dados ) => new Promise( ( resolve, reject ) => axios.post( url, dados ).then( response => resolve( response.data ) ) );

export default class Api {
  
  async buscarBanners() {
    return await _get( `${ url }banners` );
  }

  async buscarTodosProdutos( ) {
    return await _get( `${ url }produtos` );
  }

  async buscarDetalhesProduto( id ) {
    const response = await _get( `${ url }detalhes?idProduto=${ id }` );
    return response[ 0 ];
  }

  async buscarVendedor( id ) {
    const response = await _get( `${ url }vendedor?id=${ id }` );
    return response[ 0 ];
  }

  buscarCategorias(  ) {
    return _get( `${ url }categorias` );
  }
  
}