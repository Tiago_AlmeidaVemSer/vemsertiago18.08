console.log('----------exercicio 1 ----------');

const anoAtual = new Date().getFullYear();

function filtraNullOuUndefined( array ) {
    var array2 = [];
    for ( var i = 0; i < array.length; i++ ) {
        Object.values(array[ i ]).forEach(function( item ){
            if (item === null || item == '') {
                array2.push(array[ i ].titulo);
            }
        })
    }
return array2;
}

var verificaAno = (array) => {
    return array.reduce((acumula,atual) => {
    if ( atual.anoEstreia > anoAtual ) {
        acumula.push(atual.titulo)
    }
    return acumula
 }, []);
}

Array.prototype.invalidas = () => {
    const filtradosNullUndefined = filtraNullOuUndefined(series);
    const filtradosAnoInvalido = verificaAno(series);
    const invalidas = filtradosNullUndefined.concat(filtradosAnoInvalido).join(', ') ;
    return invalidas;
}

console.log(`series invalidas: ${ series.invalidas() }`);

console.log('----------exercicio 2 ----------');

Array.prototype.filtrarPorAno = (year) => series.filter(ano => ano.anoEstreia >= year );

const ano = series.filtrarPorAno(2017);

console.log(ano);

console.log('----------exercicio 3 ----------');

Array.prototype.procurarPorNome = (nome) => {
    const elenco = series.filter(serie => serie.elenco.includes(nome));
    return elenco != 0;
}

const checkName = series.procurarPorNome("Tiago Almeida");
const checkNameInvalido = series.procurarPorNome("NomeInvalido Almeida");
console.log(checkName);
console.log(checkNameInvalido);

console.log('----------exercicio 4 ----------');

Array.prototype.mediaDeEpisodios = () => {
    const total = series.reduce( (acumula, atual) => acumula + atual.numeroEpisodios, 0);
    const numeroSeries = series.length;
    return  total / numeroSeries;
}

console.log(series.mediaDeEpisodios());

console.log('----------exercicio 5 ----------');


