import React, { Component } from 'react';
import './App.css';

import ListaEpisodios from '../models/listaEpisodios';

export default class App extends Component {
  constructor( props ) {
    super(props);
    this.listaEpisodios = new ListaEpisodios();
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios
    };
  }

  render() {
     return (
      <div className="App">
          <header>
          <h2>{ this.state.episodio.nome }</h2>
          
          </header>
        </div>
    )
  };

}
