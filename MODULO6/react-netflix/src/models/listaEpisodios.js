function _sortear (min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor( Math.random() * (max - min) ) + min;
}

export default class ListaEpisodios {
    constructor() {
        this._todos = [
            { nome: 'Arkangel', duracao: 52, temporada: 4, ordemEpisodio: 2 },
            { nome: 'big', duracao: 52, temporada: 4, ordemEpisodio: 2 },
            { nome: 'hard', duracao: 52, temporada: 4, ordemEpisodio: 2 },
            { nome: 'testa', duracao: 52, temporada: 4, ordemEpisodio: 2 },
            { nome: 'dedo', duracao: 52, temporada: 4, ordemEpisodio: 2 },
            { nome: 'anel', duracao: 52, temporada: 4, ordemEpisodio: 2 },
            { nome: 'vaca', duracao: 52, temporada: 4, ordemEpisodio: 2 }
        ]
    }

    get episodiosAleatorios() {
        const indice = _sortear(0, this._todos.length)
        return this._todos[indice];
    }
}