
class Jedi {
    // _classePersonagem = "jedi";

    constructor(nome){
        this._nome = nome;
        this._estaMorto = false;
    }

    matarJedi() {
        this._estaMorto = true;
    }

    atacarComSabre(){
        setTimeout( () => {
            console.log("ataquei");
        }, 2000 );
        
    }

    get retornarNome(){
        return this._nome;
    }

    get estaMorto(){
        return this._estaMorto;
    }
    
}

let sky = new Jedi("Luke");
sky.atacarComSabre(); 
sky.matarJedi();

if(sky.estaMorto){
    console.log("Ele morreeeeeu");
}