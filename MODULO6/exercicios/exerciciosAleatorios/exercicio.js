
// *******************************EXERCÍCIO-01*****************************************

function calcularCirculo( {raio, raioDaCircunferencia, tipoCalculo} ){
    if (tipoCalculo == "A") 
        return Math.PI * (raio * raio);
    else
    if (tipoCalculo == "C") 
        return 2 * Math.PI * raio;
    else
    return "tipo calculo invalido";
}

const objetoA = {
    raio:10, 
    raioDaCircunferencia:20,
    tipoCalculo: 'A'    
}

const objetoC = {
    raio:10, 
    raioDaCircunferencia:20,
    tipoCalculo: 'C'    
}

const objetoDeuRuim = {
    raio:10, 
    raioDaCircunferencia:20,
    tipoCalculo: 'D'    
}

let testeA = calcularCirculo(objetoA);
let testeC = calcularCirculo(objetoC);
let testeDeuRuim = calcularCirculo(objetoDeuRuim);

console.log(testeA);
console.log(testeC);
console.log(testeDeuRuim);

// ******************************EXERCÍCIO-02******************************

function naoBissexto(ano){
    if(ano % 400 == 0)
        return false;
    else
        if(ano % 4 == 0 && ano % 100 != 0)
            return false;
        else
        return true;
}
let bis = naoBissexto(2020);
let nBis = naoBissexto(2021);
console.log(bis);
console.log(nBis);

// ********************************EXERCÍCIO-03***********************************

function somarPares( array ){
    let total = 0;
    for (let i = 0; i < array.length; i++) {
        if (i % 2 == 0) {
            total += array[i];
        }
    }
    return total;
}
let array = [2, 5, 1, 2,0,8,3];
console.log(somarPares(array));

// **********************************EXERCÍCIO-04****************************************

let adiciona = function(num1){
    return function(num2){
        return num1 + num2;
    };
};

console.log(adiciona(50)(20));

// ******************************************EXERCÍCIO-05******************************************

function imprimirBRL(numFlutuante){

    let resultado = "R$ "+(numFlutuante).toFixed(2);
    resultado.split('.')
    return resultado.toString();
}

console.log(imprimirBRL(0));

  