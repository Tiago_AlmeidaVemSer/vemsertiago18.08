function cardapioIFood( isVeggie = true, comLactose = true ) {
  let cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ];

  if ( comLactose ) {
    cardapio.push( 'pastel de queijo' );
  }

  cardapio = [...cardapio,
    'pastel de carne',
    'empada de legumes marabijosa'
  ];

  if ( isVeggie ) {
    cardapio.splice( cardapio.indexOf( "enroladinho de salsicha" ), 1 );    
    cardapio.splice( cardapio.indexOf( "pastel de carne" ), 1 );   
  }
  
  return cardapio.map(name => name.toUpperCase());
}

console.log(cardapioIFood());