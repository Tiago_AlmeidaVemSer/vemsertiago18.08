function cardapioIFood( isVeggie = true, comLactose = true ) {
  let cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ];

  if ( comLactose ) {
    cardapio.push( 'pastel de queijo' );
  }

  cardapio = cardapio.concat( [
    'pastel de carne',
    'empada de legumes marabijosa'
  ] );

  if ( isVeggie ) {
    const indiceEnroladinho = cardapio.indexOf( 'enroladinho de salsicha' );
    let arr = cardapio.splice( indiceEnroladinho, 1 );
    const indicePastelCarne = cardapio.indexOf( 'pastel de carne' );
    arr = cardapio.splice( indicePastelCarne, 1 );
  }
  
  return cardapio.map(name => name.toUpperCase());
}

console.log(cardapioIFood());