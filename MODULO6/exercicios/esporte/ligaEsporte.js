
// ----------------------Time----------------------------

class Time {

    jogadores = [];

    constructor(nome,tipoEsporte,status,liga){
        this._nome = nome;
        this._tipoEsporte = tipoEsporte;
        this._status = status;
        this._liga = liga;
        this._jogadores = [];
    }

    get nome() {
        return this._nome;
    }

    set nome(nome) {
        this._nome = nome; 
    }

    get tipoEsporte() {
        return this._tipoEsporte;
    }

    set tipoEsporte(tipoEsporte) {
        this._tipoEsporte = tipoEsporte;
    }

    get status() {
        return this._status;
    }

    set status(status) {
        this._status = status;
    }

    get liga() {
        return this._liga;
    }

    set liga(liga) {
        this._liga = liga;
    }

    get jogadores() {
        return this._jogadores;
    }

    set jogadores(jogadores) {
        this._jogadores;
    }

    adicionarJogador(jogador) {
        this._jogadores.push(jogador);
    }

    buscarJogadorPorNome(jogadorNome) {        
        return this._jogadores.filter( jogador => jogador.nome === jogadorNome);
    }

    buscarJogadorPorNumero(jogadorNumero){
        return this._jogadores.filter( jogador => jogador.numero === jogadorNumero);
    }
}

// --------------------------jogador------------------------------

class Jogador {

    constructor(nome, numero) {
        this._nome = nome;
        this._numero = numero;
    }

    get nome() {
        return this._nome;
    }

    set nome(nome) {
        this._nome = nome; 
    }

    get numero() {
        return this._numero;
    }

    set numero(numero) {
        this._numero = numero;
    }

}

// -----------------------partida----------------------------

class Partida {

    constructor (timeA, timeB) {
        this._timeA = timeA;
        this._timeB = timeB;
    }

    get timeA() {
        return this._timeA;
    }

    set timeA(timeA) {
        this._timeA = timeA;
    }

    get timeB() {
        return this._timeB;
    }

    set timeB(timeB) {
        this._timeB = timeB;
    }

    placar(golA, golB){
        return `${this._timeA.nome}: ${golA} x ${golB} :${this._timeB.nome}`;
    }

}

// -----------------------historico-----------------------------


class Historico {
    constructor() {
        this._partidas = [];
        this.partidasDeUmTime = [];
    }

    get partidas() {
        return this._partidas;
    }

    set partidas(partidas) {
        this._partidas = partidas;
    }

    adicionar(partida) {
        this._partidas.push(partida);
    }

    todasPartidas(){
        return console.log(this._partidas);
    }

    partidasTimeEspecifico(timeEspecifico){

        return this._partidas.filter( jogo => jogo.indexOf(timeEspecifico) >= 0 );
    }

}

// ------------------------testes----------------------------------

let tabajara = new Time("tabajara","futebol","classificado","liga prata");
let caidos = new Time("caidos","futebol","classificado","liga prata");
let meiaNoite = new Time("meiaNoite","futebol","classificado","liga prata");

let romario = new Jogador("romario",11);
let batistuta = new Jogador("Batistuta", 9);
let maradona = new Jogador("Maradona",10);

tabajara.adicionarJogador(romario);
tabajara.adicionarJogador(batistuta);
tabajara.adicionarJogador(maradona);

let partida = new Partida(tabajara,caidos);
let partida2 = new Partida(tabajara,meiaNoite);
let partida3 = new Partida(meiaNoite,caidos);

let historico = new Historico();


let buscarNome = tabajara.buscarJogadorPorNome("romario");
let buscarNumero = tabajara.buscarJogadorPorNumero(10);
console.log(buscarNome);
console.log(buscarNumero);

let placar = partida.placar(2,1);
let placar2 = partida2.placar(2,2);
let placar3 = partida3.placar(2,3);

historico.adicionar(placar);
historico.adicionar(placar2);
historico.adicionar(placar3);

historico.todasPartidas();

console.log(placar);

console.log(historico.partidasTimeEspecifico("tabajara"));