
const pokeApi = new PokeApi();
// eslint-disable-next-line no-unused-vars
let idNaTela = 0;
const pokemonsGerados = [];

// eslint-disable-next-line no-unused-vars
function buscarPorId() {
  const idPokemon = document.getElementById( 'idPokemon' ).value;
  const erro = document.getElementById( 'erro' );
  pokeApi
    .buscarEspecifico( idPokemon )
    .then( pokemon => {
      erro.innerHTML = '';
      const poke = new Pokemon( pokemon )
      // eslint-disable-next-line no-use-before-define
      renderizar( poke );
    },
    err => {
      // eslint-disable-next-line no-shadow
      const erro = document.getElementById( 'erro' );
      erro.innerHTML = 'Digite um id válido';
      console.log( err );
    } );
}
// eslint-disable-next-line no-unused-vars
function buscarOnChange() {
  const erro = document.getElementById( 'erro' );
  const idPokemon = document.getElementById( 'idPokemon' ).value;
  pokeApi
    .buscarEspecifico( idPokemon )
    .then( () => {
      erro.innerHTML = '';
    },
    err => {
      erro.innerHTML = 'Digite um id válido';
      console.log( err );
    } );
}
// eslint-disable-next-line no-unused-vars
function buscarComSorte() {
  // eslint-disable-next-line no-undef
  randomNumber = ( min, max ) => Math.floor( Math.random() * ( max - min + 1 ) ) + min
  // eslint-disable-next-line no-undef
  const idBusca = randomNumber( 1, 893 );
  // eslint-disable-next-line eqeqeq
  const numJafoi = pokemonsGerados.filter( num => num == idBusca );
  if ( numJafoi.length > 0 ) {
    return;
  }
  pokemonsGerados.push( idBusca );

  pokeApi
    .buscarEspecifico( idBusca )
    .then( pokemon => {
      // eslint-disable-next-line no-undef
      erro.innerHTML = '';
      const poke = new Pokemon( pokemon )
      // eslint-disable-next-line no-use-before-define
      renderizar( poke );
    },
    err => {
      const erro = document.getElementById( 'erro' );
      erro.innerHTML = 'Digite um id válido';
      console.log( err );
    } );
}

function renderizar( pokemon ) {
  const dadosPokemon = document.getElementById( 'pokedex' );
  const nome = dadosPokemon.querySelector( '.nome' );
  nome.innerHTML = `Nome: ${ pokemon.nome }`;

  const id = dadosPokemon.querySelector( '.id' );
  id.innerHTML = `Id: ${ pokemon.id }`;
  idNaTela = pokemon.id;

  const imgPokemon = dadosPokemon.querySelector( '.thumb' );
  imgPokemon.src = pokemon.imagem;

  const altura = dadosPokemon.querySelector( '.altura' );
  altura.innerHTML = `Altura: ${ pokemon.altura }`;

  const peso = dadosPokemon.querySelector( '.peso' );
  peso.innerHTML = `Peso: ${ pokemon.peso }`;

  // ---------------Tipos
  const tipos = dadosPokemon.querySelector( '.tipo' );
  tipos.innerHTML = 'Tipo(s):'
  const ul = document.createElement( 'ul' );
  tipos.appendChild( ul );
  // eslint-disable-next-line array-callback-return
  pokemon.tipos.map( tipo => {
    const li = document.createElement( 'li' );
    li.innerHTML = `${ tipo.type.name } `;
    ul.appendChild( li );
  } );

  // -------------Estatistica
  const estatistica = dadosPokemon.querySelector( '.estatistica' );
  estatistica.innerHTML = 'Estatísticas:';
  const ulStat = document.createElement( 'ul' );
  estatistica.appendChild( ulStat );
  // eslint-disable-next-line array-callback-return
  pokemon.estatistica.map( stats => {
    const li = document.createElement( 'li' );
    li.innerHTML = `
        Nome: ${ stats.stat.name }
        * Valor base:  ${ stats.base_stat }%
        * Esforço: ${ stats.effort }%
        `;
    ulStat.appendChild( li );
  } );
}
