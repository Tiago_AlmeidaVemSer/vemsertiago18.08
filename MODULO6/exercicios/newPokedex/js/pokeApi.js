class PokeApi { // eslint-disable-line no-unused-vars
  constructor() {
    this._api = 'https://pokeapi.co/api/v2/pokemon';
  }

  buscarTodos() {
    const fazRequisicao = fetch( `${ this._api }?limit=1050&offset=0` );
    return fazRequisicao.then( resultadoEmString => resultadoEmString.json() );
  }

  buscarEspecifico( id ) {
    const fazRequisicao = fetch( `${ this._api }/${ id }` );
    return fazRequisicao.then( resultadoEmString => resultadoEmString.json() );
  }
}
