import React from 'react'

export default function Footer() {
    return (
        <React.Fragment>
            <footer>
                <p>&copy 1997-2020 Netflix, Inc.</p>
                <p>Tiago Almeida &copy 2020</p>
            </footer>
        </React.Fragment>
    )
}