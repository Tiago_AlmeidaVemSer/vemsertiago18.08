import React from 'react';
import './header.css'

export default function Header() {

    return (
        <React.Fragment>
            <header>
            <div class="netflixLogo">
                <a id="logo" href="http://localhost:3000/"><img src="https://github.com/carlosavilae/Netflix-Clone/blob/master/img/logo.PNG?raw=true" alt="Logo Image"/></a>
            </div>
            <nav class="main-nav">                
                <a class="btnRanking" href="http://localhost:3000/avaliacoes">Ranking</a>
                
            </nav>
            <nav class="sub-nav">
                <a href="#"><i class="fas fa-search sub-nav-logo"></i></a>
                <a href="#"><i class="fas fa-bell sub-nav-logo"></i></a>
                <a href="https://www.netflix.com/br/login">Account</a>    
            </nav> 
            </header>
        </React.Fragment>
    )
   
}