import React from 'react'

export default function Body() {

    return (
        <React.Fragment>
            <section class="main-container" >
        <div class="location" id="home">
          <h1 id="home">Black Mirror</h1>
          <div class="box">
            <a href="http://localhost:3000/episodio/1"><img src="https://occ-0-678-559.1.nflxso.net/art/9aeb1/6f3ebb055fe062f85dc3f84871bdafaee6a9aeb1.jpg" alt="" /></a>
            <a href="http://localhost:3000/episodio/2"><img src="https://occ-0-678-559.1.nflxso.net/art/c4d52/ba1d86466a8785d4ba73c76cbcebe2e6384c4d52.jpg" alt="" /></a>
            <a href="http://localhost:3000/episodio/3"><img src="https://occ-0-678-559.1.nflxso.net/art/8b126/81549c8f021fc16cc4b8af1586ddc3b42d88b126.jpg" alt="" /></a>
            <a href="http://localhost:3000/episodio/4"><img src="https://occ-0-678-559.1.nflxso.net/art/557ff/558fcde2fe704bf3eaa2ef3b26aad3d38e6557ff.jpg" alt="" /></a>
            <a href="http://localhost:3000/episodio/5"><img src="https://occ-0-678-559.1.nflxso.net/art/400a9/57eb44a76c31274fef233131d583ab7bc97400a9.jpg" alt="" /></a>
            <a href="http://localhost:3000/episodio/6"><img src="https://occ-0-678-559.1.nflxso.net/art/7f50e/422928a1b9fa31b8a6c209b5df4d0915f557f50e.jpg" alt="" /></a>
    
            <a href="http://localhost:3000/episodio/7"><img src="https://occ-0-678-559.1.nflxso.net/art/61a3e/02aa15ddad05aa972d53bcad460aa9d527f61a3e.jpg" alt="" /></a>
            <a href="http://localhost:3000/episodio/8"><img src="https://occ-0-678-559.1.nflxso.net/art/6a32d/88bf30eae1ae98b82b2e8081813e731ab0c6a32d.jpg" alt="" /></a>
            <a href="http://localhost:3000/episodio/9"><img src="https://occ-0-678-559.1.nflxso.net/art/3940b/4c0badc82119dc19439e181f19234c074a83940b.jpg" alt="" /></a>
            <a href="http://localhost:3000/episodio/10"><img src="https://occ-0-678-559.1.nflxso.net/art/ab4ae/08cf3abd7ddbef2acf82350fad6ee9ebfedab4ae.jpg" alt="" /></a>
            <a href="http://localhost:3000/episodio/11"><img src="https://occ-0-678-559.1.nflxso.net/art/68242/db5699eda3c612123fed50166ef9598b14068242.jpg" alt="" /></a>
            <a href="http://localhost:3000/episodio/12"><img src="https://occ-0-678-559.1.nflxso.net/art/424df/608de21c697b612e00ef851052ccba447c7424df.jpg" alt="" /></a>        
            <a href="http://localhost:3000/episodio/13"><img src="https://occ-0-678-559.1.nflxso.net/art/75542/cc3e412d8e66f8f4de0476eb37a23a63b0875542.jpg" alt="" /></a>        
            <a href="http://localhost:3000/episodio/14"><img src="https://occ-0-678-559.1.nflxso.net/art/69c24/ecfc0c072506955c45aa259293f1a2f5c9869c24.jpg" alt="" /></a>        
            <a href="http://localhost:3000/episodio/15"><img src="https://occ-0-678-559.1.nflxso.net/art/89271/5e8b75f4582aac66f780664a86025992b4a89271.jpg" alt="" /></a>        
            <a href="http://localhost:3000/episodio/16"><img src="https://occ-0-678-559.1.nflxso.net/art/8e8ef/c08aaf2ae6c9daa52320b41bef71a26921d8e8ef.jpg" alt="" /></a>        
            <a href="http://localhost:3000/episodio/17"><img src="https://occ-0-678-559.1.nflxso.net/art/f5694/edcafaec5cd271131b245254c3106f7cd92f5694.jpg" alt="" /></a>        
            <a href="http://localhost:3000/episodio/18"><img src="https://occ-0-678-559.1.nflxso.net/art/abf16/6090599058bb80cae847951d5273f00e874abf16.jpg" alt="" /></a>        
            <a href="http://localhost:3000/episodio/19"><img src="https://occ-0-678-559.1.nflxso.net/art/0b465/db9e48d9e27a99396cb9202601159454f6e0b465.jpg" alt="" /></a>        
          </div>
      </div> 
      </section>    
    
    <section class="link">
      <div class="logos">
        <a href="#"><i class="fab fa-facebook-square fa-2x logo"></i></a>
        <a href="#"><i class="fab fa-instagram fa-2x logo"></i></a>
        <a href="#"><i class="fab fa-twitter fa-2x logo"></i></a>
        <a href="#"><i class="fab fa-youtube fa-2x logo"></i></a>
      </div>
      <div class="sub-links">
        <ul>
          <li><a href="#">Audio and Subtitles</a></li>
          <li><a href="#">Audio Description</a></li>
          <li><a href="#">Help Center</a></li>
          <li><a href="#">Gift Cards</a></li>
          <li><a href="#">Media Center</a></li>
          <li><a href="#">Investor Relations</a></li>
          <li><a href="#">Jobs</a></li>
          <li><a href="#">Terms of Use</a></li>
          <li><a href="#">Privacy</a></li>
          <li><a href="#">Legal Notices</a></li>
          <li><a href="#">Corporate Information</a></li>
          <li><a href="#">Contact Us</a></li>
        </ul>
      </div>
    </section>
    
        </React.Fragment>
    )

}