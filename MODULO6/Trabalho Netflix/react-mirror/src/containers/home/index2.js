import React, { Component } from 'react';
import Header2 from '../../components/header'
import Body from '../../components/body'
import Footer from '../../components/footer'

export default function Index2()  {
    return (
                <React.Fragment>
                  <Header2 />
                  <Body />
                  <Footer />            
                </React.Fragment>
              
    )
}