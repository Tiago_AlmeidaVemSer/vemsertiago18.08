const NomeContext = React.createContext( 'nome' );

        function MeuComponente1(props) {
            return (
                    <div className="componente-1">
                        <MeuComponente2>
                            <MeuComponente3 onClickIncrementar={props.onClickIncrementar} />
                        </MeuComponente2>
                    </div>
            )
        }
        
        function MeuComponente2(props) {         

            return (
                <div className="componente-2">
                    <div>
                        <header></header>
                        <footer>{props.children}</footer>
                    </div>
                </div>
            )
        }

        function MeuComponente3(props) {
            const [ telefone, setTelefone ] = React.useState('51 3625441778')

            setTimeout(() => setTelefone('51 999999999'),2000);

            return (
                <div className="componente-3">
                    < MeuComponente4 telefone={telefone} onClickIncrementar={props.onClickIncrementar}/>
                </div>
            )
        }

        function MeuComponente4(props) {
            const [ idade, setIdade ] = React.useState(32);            

            setTimeout(() => setIdade(33), 1500);

            return (
                    <div className="componentes-4">
                      <p>Componente 4 {idade} - {props.telefone}</p>
                      <button type="button" onClick={props.onClickIncrementar}>Incrementar</button>
                    </div>
                )
                }    
            
        function MeuComponente(props) {
            return (
                <div id="componentes">
                    <MeuComponente1 onClickIncrementar={props.onClickIncrementar} />
                </div>
            )
        }       

        function MeuComponenteIrmao(props) {
            return (
                <div id="componente-irmão">
                    <MeuComponenteIrmao2 contador={props.contador}/>
                </div>
            )
        }

        function MeuComponenteIrmao2(props) {

            React.useEffect(function() {
                localStorage.setItem('contador', props.contador);
            });

            return (
            <h2>Contador: {props.contador}</h2>
            )
        }

        function AppComponente() {
            const [ contador, incrementaContador ] = React.useState(parseInt(localStorage.getItem('contador'), 10) || 0);

            const clickIncrementa = function() {
                incrementaContador(contador + 1);
            }

            return (
                <React.Fragment>
                    <MeuComponente onClickIncrementar={clickIncrementa} />
                    <MeuComponenteIrmao contador={contador}/>
                </React.Fragment>
            )
        }

        ReactDOM.render(
            < AppComponente />,
            document.getElementById( 'app' )
        )