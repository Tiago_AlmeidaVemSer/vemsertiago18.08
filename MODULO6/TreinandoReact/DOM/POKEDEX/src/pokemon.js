class Pokemon {
    constructor( objDaApi ) {
        this._nome = objDaApi.name;
        this._imagem = objDaApi.sprites.front_default;
        this._altura = objDaApi.height;
    }

    get nome() {
        return this._nome;
    }

    get imagem() {
        return this._imagem;
    }

    get altura() {
        return this._altura;
    }

}