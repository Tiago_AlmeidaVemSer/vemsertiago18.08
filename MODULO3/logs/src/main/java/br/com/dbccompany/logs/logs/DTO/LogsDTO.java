package br.com.dbccompany.logs.logs.DTO;

import br.com.dbccompany.logs.logs.Entity.LogsEntity;

public class LogsDTO {

    private Integer id;
    private String codigo;
    private String mensagem;

    public LogsDTO(){}

    public LogsDTO(LogsEntity entity){
        this.id = entity.getId();
        this.codigo = entity.getCodigo();
        this.mensagem = entity.getMensagem();
    }

    public LogsEntity convert(){
        LogsEntity logs = new LogsEntity();
        logs.setId(this.id);
        logs.setCodigo(this.codigo);
        logs.setMensagem(this.mensagem);

        return logs;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

}
