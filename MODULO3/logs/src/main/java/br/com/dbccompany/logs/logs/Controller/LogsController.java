package br.com.dbccompany.logs.logs.Controller;

import br.com.dbccompany.logs.logs.DTO.LogsDTO;
import br.com.dbccompany.logs.logs.Entity.LogsEntity;
import br.com.dbccompany.logs.logs.Service.LogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/logs")
public class LogsController{

    @Autowired
    LogsService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<LogsDTO> todosLogs(){
        return service.retornarListaLogs();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public LogsEntity salvar(@RequestBody LogsDTO dto){
        return service.salvarLogs(dto);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public LogsDTO clientesEspecifico(@PathVariable Integer id){
        return service.retonarLogsEspecificos(id);
    }

}
