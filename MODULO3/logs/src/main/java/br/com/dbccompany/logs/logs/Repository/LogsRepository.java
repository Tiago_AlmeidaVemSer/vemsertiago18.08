package br.com.dbccompany.logs.logs.Repository;

import br.com.dbccompany.logs.logs.Entity.LogsEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogsRepository extends CrudRepository<LogsEntity, Integer> {

    LogsEntity findByCodigo(String codigo);
    LogsEntity findByMensagem(String mensagem);

}
