package br.com.dbccompany.logs.logs.Entity;

import javax.persistence.*;

@Entity
public class LogsEntity {

    @Id
    @SequenceGenerator(name = "LOGS_SEQ", sequenceName = "LOGS_SEQ")
    @GeneratedValue(generator = "LOGS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String codigo;
    private String mensagem;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
