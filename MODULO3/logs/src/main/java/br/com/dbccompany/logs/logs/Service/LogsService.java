package br.com.dbccompany.logs.logs.Service;

import br.com.dbccompany.logs.logs.DTO.LogsDTO;
import br.com.dbccompany.logs.logs.Entity.LogsEntity;
import br.com.dbccompany.logs.logs.LogsApplication;
import br.com.dbccompany.logs.logs.Repository.LogsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogsService {

    @Autowired
    private LogsRepository repository;

    protected Logger logger = LoggerFactory.getLogger(LogsApplication.class);

    @Transactional(rollbackFor = Exception.class)
    public LogsEntity salvar(LogsEntity entity){
        try{
            logger.warn("salvando entidade");
            return repository.save(entity);
        }catch(Exception e){
            logger.error("Erro ao tentar salvar entidade");
            return null;
        }
    }

    public List<LogsEntity> todos() {
        try{
            logger.warn("buscando entidades");
            return (List<LogsEntity>) repository.findAll();
        }catch(Exception e){
            logger.error("Erro ao tentar buscar todas entidades");
            return null;
        }
    }

    public LogsEntity porId( Integer id) {
        try{
            logger.warn("buscando Entidade por id");
            return repository.findById(id).get();
        }catch(Exception e){
            logger.error("Erro ao tentar buscar por id");
            return null;
        }
    }

    public List<LogsDTO> retornarListaLogs() {
        try {
            logger.warn("retornando lista de logs");
            List<LogsDTO> listaDTO = new ArrayList<>();
            for (LogsEntity logs : this.todos()) {
                listaDTO.add(new LogsDTO(logs));
            }
            return listaDTO;
        } catch (Exception e) {
            logger.error("Erro ao retornar lista ");
            return null;
        }
    }

    public LogsEntity salvarLogs(LogsDTO dto){
        try{
            logger.warn("salvando logs");
            LogsEntity logs = this.salvar(dto.convert());
            return logs;
        }catch(Exception e){
            logger.error("Erro ao tentar salvar logs");
            return null;
        }
    }

    public LogsDTO retonarLogsEspecificos(Integer id){
        try{
            logger.warn("retornando logs especificos");
            LogsDTO logs = new LogsDTO(this.porId(id));
            return logs;
        }catch(Exception e){
            logger.error("Erro ao tentar retornar logs especificos");
            return null;
        }
    }

}
