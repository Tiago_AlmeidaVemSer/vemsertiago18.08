package br.com.dbccompany.logs.hackathon.Dominio;

public class Vendas {

    public String saleId;
    public String item;
    public String idItem;
    public String qtdItem;
    public String price;
    public String SalesmanName;

    public String getSaleId() {
        return saleId;
    }

    public void setSaleId(String saleId) {
        this.saleId = saleId;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getIdItem() {
        return idItem;
    }

    public void setIdItem(String idItem) {
        this.idItem = idItem;
    }

    public String getQtdItem() {
        return qtdItem;
    }

    public void setQtdItem(String qtdItem) {
        this.qtdItem = qtdItem;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSalesmanName() {
        return SalesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        SalesmanName = salesmanName;
    }
}
