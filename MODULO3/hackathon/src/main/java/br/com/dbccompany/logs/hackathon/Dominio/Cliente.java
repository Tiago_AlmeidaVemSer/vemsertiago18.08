package br.com.dbccompany.logs.hackathon.Dominio;

public class Cliente {

    public String cnpj;
    public String name;
    public String businesArea;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBusinesArea() {
        return businesArea;
    }

    public void setBusinesArea(String businesArea) {
        this.businesArea = businesArea;
    }
}
