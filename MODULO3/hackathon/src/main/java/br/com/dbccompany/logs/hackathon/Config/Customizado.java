package br.com.dbccompany.logs.hackathon.Config;

import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Configuration
public class Customizado {

    String path = "/home/tiago/Data/in.dat";
    String pathSaida = "/home/tiago/Data/out2.dat";

    @SuppressWarnings({"rawtypes", "unchecked"})
    @StepScope
    @Bean
    public FlatFileItemReader leituraArquivoMultiploFormato(
            @Value("#{jobParameters['arquivoDaddosDeEntrada']}") Resource arquivoDaddosDeEntrada,
            LineMapper lineMapper) {
        return new FlatFileItemReaderBuilder()
                .name("leituraArquivoMultiploFormato")
                .resource(arquivoDaddosDeEntrada)
                .lineMapper(lineMapper)
                .build();
    }

}
