package br.com.dbccompany.logs.hackathon.Config;


import br.com.dbccompany.logs.hackathon.Dominio.Cliente;
import br.com.dbccompany.logs.hackathon.Dominio.Vendas;
import br.com.dbccompany.logs.hackathon.Dominio.Vendedor;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.mapping.PatternMatchingCompositeLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class ClienteTransacaoLineMapperConfig {

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Bean
    public PatternMatchingCompositeLineMapper lineMapper(){
        PatternMatchingCompositeLineMapper lineMapper = new PatternMatchingCompositeLineMapper();
        lineMapper.setTokenizers(tokenizers());
        lineMapper.setFieldSetMappers(fieldSetMappers());
        return lineMapper;
    }

    @SuppressWarnings("rawtypes")
    private Map<String, LineTokenizer> tokenizers() {
        Map<String, LineTokenizer> tokenizers = new HashMap<>();
        tokenizers.put("001*", vendedorLineTokenizer());
        tokenizers.put("002*", ClienteLineTokenizer());
        tokenizers.put("003*", VendasLineTokenizer());
        return tokenizers;
    }

    private LineTokenizer vendedorLineTokenizer() {
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames("cpf", "name", "salary");
        lineTokenizer.setIncludedFields(1, 2, 3);
        return lineTokenizer;
    }

    private LineTokenizer ClienteLineTokenizer() {
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames("cnpj", "name", "businesArea");
        lineTokenizer.setIncludedFields(1, 2, 3);
        return lineTokenizer;
    }

    private LineTokenizer VendasLineTokenizer() {
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames("saleId", "item", "idItem", "qtdItem", "price", "salesManName");
        lineTokenizer.setIncludedFields(1, 2, 3, 4, 5, 6);
        return lineTokenizer;
    }


    @SuppressWarnings("rawtypes")
    private Map<String, FieldSetMapper> fieldSetMappers() {
        Map<String, FieldSetMapper> fieldSetMappers = new HashMap<>();
        fieldSetMappers.put("001*", fieldSetMapper(Vendedor.class));
        fieldSetMappers.put("001*", fieldSetMapper(Cliente.class));
        fieldSetMappers.put("001*", fieldSetMapper(Vendas.class));
        return fieldSetMappers;
    }
    @SuppressWarnings("rawtypes")
    private FieldSetMapper fieldSetMapper(Class classe) {
        BeanWrapperFieldSetMapper fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(classe);
        return fieldSetMapper;
    }

}
