package br.com.dbccompany.logs.hackathon.Util;

import java.util.HashMap;

public class HashMapIteratorUtil {

    public static String verificaMenorValorHashMap(HashMap<String, Double> map) {
        String retorno = "";
        for(HashMap.Entry<String, Double> entry1: map.entrySet()) {
            String key1 = entry1.getKey();
            Double value1 = entry1.getValue();
            for(HashMap.Entry<String, Double> entry2: map.entrySet()) {
                String key2 = entry2.getKey();
                Double value2 = entry2.getValue();
                if (value1 < value2) {
                    retorno = entry1.getKey();
                }else{
                    return entry2.getKey();
                }
            }
        }
        return retorno;
    }
}