package br.com.dbccompany.logs.hackathon.Dominio;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;


public class Vendedor {


    private Integer id;
    private String dado1;
    private String dado2;
    private String dado3;
    private String dado4;
    private List<Cliente> transacoes = new ArrayList<>();

    public Vendedor() {
    }

    public Vendedor(Integer id, String dado1, String dado2, String dado3, String dado4) {
        this.id = id;
        this.dado1 = dado1;
        this.dado2 = dado2;
        this.dado3 = dado3;
        this.dado4 = dado4;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDado1() {
        return dado1;
    }

    public void setDado1(String dado1) {
        this.dado1 = dado1;
    }

    public String getDado2() {
        return dado2;
    }

    public void setDado2(String dado2) {
        this.dado2 = dado2;
    }

    public String getDado3() {
        return dado3;
    }

    public void setDado3(String dado3) {
        this.dado3 = dado3;
    }

    public String getDado4() {
        return dado4;
    }

    public void setDado4(String dado4) {
        this.dado4 = dado4;
    }
}
