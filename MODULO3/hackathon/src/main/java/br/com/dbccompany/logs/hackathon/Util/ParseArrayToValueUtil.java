package br.com.dbccompany.logs.hackathon.Util;

public class ParseArrayToValueUtil {

    public static double converteParaValor(String array) {
        double valorTotal = 0;
        String auxiliar = array.replaceAll("\\[", "");
        auxiliar = auxiliar.replaceAll("\\]", "");
        String[] arrayAux = auxiliar.split(",");
        for (String aux: arrayAux) {
            String[] arr = aux.split("-");
            valorTotal += (Double.parseDouble(arr[0]) * Double.parseDouble(arr[1]) * Double.parseDouble(arr[2]));
        }
        return valorTotal;
    }

}
