package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Builder.BuildCliente;
import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClienteService extends ServiceAbstract<ClienteRepository, ClienteEntity, Integer> {
    @Autowired
    private ContatoService contatoService;

    @Autowired
    private TipoContatoService tipoContatoService;

    @Transactional( rollbackFor = Exception.class )
    public ClienteEntity salvar(ClienteDTO dto) {

        checaSeExisteTipoTelefoneEEmail();

        ClienteEntity cliente = super.salvar(BuildCliente.entidade(dto));

        contatoService.salvar(new ContatoEntity(
                tipoContatoService.buscarPorTipo("email"),
                cliente,
                dto.getEmailValor()
        ));

        contatoService.salvar(new ContatoEntity(
                tipoContatoService.buscarPorTipo("telefone"),
                cliente,
                dto.getTelefoneValor()
        ));

        return cliente;
    }

    @Transactional
    public ClienteEntity editar(ClienteDTO dto, int id) {
        ClienteEntity cliente = BuildCliente.entidade(dto);
        cliente.setId(id);
        return repository.save(cliente);
    }

    public List<ClienteDTO> todosClientes() {
        List<ClienteEntity> clientes  = super.todos();
        List<ClienteDTO> dtos = new ArrayList<>();
        for (ClienteEntity c : clientes) {
            dtos.add(BuildCliente.DTO(c));
        }
        return dtos;
    }


    public ClienteEntity buscarPorCpf(String cpf) {
        return repository.findByCpf(cpf);
    }

    private void checaSeExisteTipoTelefoneEEmail() {
        Optional<TipoContatoEntity> email = Optional.ofNullable(tipoContatoService.buscarPorTipo("email"));
        Optional<TipoContatoEntity> telefone = Optional.ofNullable(tipoContatoService.buscarPorTipo("telefone"));
        if (!email.isPresent()) {
            tipoContatoService.salvar(new TipoContatoEntity("email"));
        }
        if (!telefone.isPresent()) {
            tipoContatoService.salvar(new TipoContatoEntity("telefone"));
        }
    }
}
