package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Enum.TipoPagamentoEnum;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = PagamentoEntity.class)
public class PagamentoEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ")
    @GeneratedValue( generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE_PACOTE")
    private ClientePacoteEntity clientePacote;

    @ManyToOne
    @JoinColumn(name = "ID_CONTRATACAO", nullable = false)
    private ContratacaoEntity contratacao;

    @Enumerated(EnumType.STRING)
    private TipoPagamentoEnum tipoPagamento;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public ClientePacoteEntity getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacoteEntity clientePacote) {
        this.clientePacote = clientePacote;
    }
}
