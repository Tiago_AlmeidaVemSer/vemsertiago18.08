package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import br.com.dbccompany.coworking.Service.PagamentoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("api/pagamentos")
public class PagamentoController {

    @Autowired
    private PagamentoService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @PostMapping("/novo")
    @ResponseBody
    public PagamentoEntity salvar(@RequestBody PagamentoDTO dto) {
        logger.info("requisicao (POST) api/pagamento/novo sendo processada, data: " + LocalDate.now());
        try {

            return service.salvar(dto);

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @GetMapping(value = "buscar/todos")
    @ResponseBody
    public List<PagamentoEntity> todos() {
        logger.info("requisicao (GET) api/pagamento/todos sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja pagamentos registrados");

        return service.todos();
    }
