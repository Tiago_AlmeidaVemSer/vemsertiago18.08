package br.com.dbccompany.coworking.Builder;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;

public class BuildClientesPacotes {
    public ClientePacoteEntity construir(ClienteEntity cliente, PacoteEntity pacote, int quantidade) {
        ClientePacoteEntity clientesPacotes = new ClientePacoteEntity();
        clientesPacotes.setCliente(cliente);
        clientesPacotes.setPacote(pacote);
        clientesPacotes.setQuantidade(quantidade);
        return clientesPacotes;
    }
}
