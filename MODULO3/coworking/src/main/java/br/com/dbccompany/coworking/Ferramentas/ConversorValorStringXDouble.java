package br.com.dbccompany.coworking.Ferramentas;

public class ConversorValorStringXDouble {


    public static String converterParaString(double valor) {
         return "R$ " + valor;
    }

    public static double converterParaDouble(String valor) {
        String [] valorEmString = valor.split(" ");
        return Double.parseDouble(valorEmString[1]);
    }
}
