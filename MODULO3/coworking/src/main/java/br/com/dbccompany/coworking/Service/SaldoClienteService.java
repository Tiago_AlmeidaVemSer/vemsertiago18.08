package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.springframework.stereotype.Service;

@Service
public class SaldoClienteService extends ServiceAbstract<SaldoClienteRepository, SaldoClienteEntity, SaldoClienteId> {

}
