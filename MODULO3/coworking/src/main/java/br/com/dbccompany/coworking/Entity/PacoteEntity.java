package br.com.dbccompany.coworking.Entity;


import javax.persistence.*;
import java.util.List;

@Entity
public class PacoteEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "PACOTES_SEQ", sequenceName = "PACOTES_SEQ")
    @GeneratedValue( generator = "PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @OneToMany( mappedBy = "pacote")
    private List<EspacoPacoteEntity> espacosPacotes;

    private double valor;

    @OneToMany( mappedBy = "pacote")
    private List<ClientePacoteEntity> clientesPacotes;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public List<EspacoPacoteEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacoPacoteEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<ClientePacoteEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientePacoteEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }
}
