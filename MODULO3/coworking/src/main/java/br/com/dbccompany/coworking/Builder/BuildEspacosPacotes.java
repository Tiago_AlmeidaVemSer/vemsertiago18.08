package br.com.dbccompany.coworking.Builder;

import br.com.dbccompany.cowork.DTO.ContratacaoPacoteDTO;
import br.com.dbccompany.cowork.Entity.EspacoEntity;
import br.com.dbccompany.cowork.Entity.EspacoPacoteEntity;
import br.com.dbccompany.cowork.Entity.PacoteEntity;

public class BuildEspacosPacotes {
    public EspacoPacoteEntity construir(EspacoEntity espaco, PacoteEntity pacotePersistido, ContratacaoPacoteDTO dto) {
        EspacoPacoteEntity espacosPacotes = new EspacoPacoteEntity();
        espacosPacotes.setEspaco(espaco);
        espacosPacotes.setPacote(pacotePersistido);
        espacosPacotes.setPrazo(dto.getPrazo());
        espacosPacotes.setQuantidade(dto.getQuantidade());
        espacosPacotes.setTipoContratacao(dto.getTipoContratacao());
        return espacosPacotes;
    }
}
