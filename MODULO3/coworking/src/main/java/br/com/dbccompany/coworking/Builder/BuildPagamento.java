package br.com.dbccompany.coworking.Builder;

import br.com.dbccompany.cowork.DTO.PagamentoDTO;
import br.com.dbccompany.cowork.Entity.PagamentoEntity;

public class BuildPagamento {
    public static PagamentoEntity construir(PagamentoDTO dto) {
        PagamentoEntity pagamento = new PagamentoEntity();
        pagamento.setTipoPagamento(dto.getTipoPagamento());
        return pagamento;
    }
}
