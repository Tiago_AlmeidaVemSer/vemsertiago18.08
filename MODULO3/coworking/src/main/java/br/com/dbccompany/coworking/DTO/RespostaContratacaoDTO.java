package br.com.dbccompany.coworking.DTO;

public class RespostaContratacaoDTO {

    double valor;

    public RespostaContratacaoDTO() {
    }

    public RespostaContratacaoDTO(double valor) {
        this.valor = valor;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
