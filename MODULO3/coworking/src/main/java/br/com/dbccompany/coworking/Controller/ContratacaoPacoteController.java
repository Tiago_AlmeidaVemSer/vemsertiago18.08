//package br.com.dbccompany.coworking.Controller;
//
//
//import br.com.dbccompany.coworking.Builder.*;
//import br.com.dbccompany.coworking.DTO.ContratacaoPacoteDTO;
//import br.com.dbccompany.coworking.Entity.*;
//import br.com.dbccompany.coworking.Service.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//@Controller
//@RequestMapping("/api/contratacaopacote/")
//public class ContratacaoPacoteController {
//
//    @Autowired
//    private PacotesService pacotesService;
//
//    @Autowired
//    private EspacosService espacosService;
//
//    @Autowired
//    private ContratacaoService contratacaoService;
//
//    @Autowired
//    private ClientePacoteService clientesPacotesService;
//
//    @Autowired
//    private EspacoPacotesService espacoPacotesService;
//
//    @Autowired
//    private PagamentosService pagamentosService;
//
//    @Autowired
//    private ClientesService clientesService;
//
//    @PostMapping
//    public ResponseEntity<String> cadastrar(@RequestBody ContratacaoPacoteDTO dto) {
//
//        try {
//            EspacoEntity espaco = espacosService.porId(dto.getIdEspacos());
//
//            ClienteEntity cliente = clientesService.porId(dto.getIdCliente());
//
//            PacoteEntity pacotePersistido = pacotesService
//                    .salvar(new BuildPacote()
//                            .construir(espaco.getValor() * dto.getQuantidade()));
//
//            ClientePacoteEntity clientesPacotesPersistido = clientesPacotesService
//                    .salvar(new BuildClientesPacotes()
//                            .construir(
//                                    cliente,
//                                    pacotePersistido,
//                                    dto.getQuantidade()
//                            ));
//
//            ContratacaoEntity contratacaoPersistida = contratacaoService
//                    .salvar(new BuildContratacao()
//                            .construir(
//                                    dto,
//                                    espaco,
//                                    cliente
//                            ));
//
//            PagamentoEntity pagamentoPersistido = pagamentosService
//                    .salvar(new BuildPagamento()
//                            .construir(
//                                    clientesPacotesPersistido,
//                                    dto,
//                                    contratacaoPersistida
//                            ));
//
//            EspacoPacoteEntity espacosPacotesPersistido = espacoPacotesService
//                    .salvar( new BuildEspacosPacotes()
//                            .construir(
//                                    espaco,
//                                    pacotePersistido,
//                                    dto
//                            ));
//
//            return ResponseEntity.ok().body("Contratacao realizada com sucesso!");
//
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//            return ResponseEntity.badRequest().build();
//        }
//    }
//}
