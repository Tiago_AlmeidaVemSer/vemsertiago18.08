package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.Repository.UsuarioRepository;
import br.com.dbccompany.coworking.Security.Cryptography;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsuarioService extends ServiceAbstract<UsuarioRepository, UsuarioEntity, Integer> {

    @Override
    @Transactional( rollbackFor = Exception.class )
    public UsuarioEntity salvar(UsuarioEntity entity) {

        String password = entity.getSenha();

        if(Cryptography.checkPassword(password)){
            entity.setSenha(new BCryptPasswordEncoder().encode(entity.getPassword()));
            return this.repository.save(entity);
        }
        return null;
    }

    public UsuarioEntity porEmail(String email) {
        return repository.findByEmail(email);
    }

    public UsuarioEntity porLogin(String login) {
        return repository.findByLogin(login).get();
    }

    @Override
    public UsuarioEntity editar(UsuarioEntity entidade, Integer id) {
        entidade.setId(id);
        repository.save(entidade);
        return entidade;
    }

}
