package br.com.dbccompany.coworking.DTO;

public class NomeEspacoDTO {

    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
