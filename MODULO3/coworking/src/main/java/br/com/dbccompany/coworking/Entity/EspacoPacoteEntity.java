package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Enum.TipoContratacaoEnum;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EspacoPacoteEntity.class)
public class EspacoPacoteEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "ESPACO_PACOTE_SEQ", sequenceName = "ESPACO_PACOTE_SEQ")
    @GeneratedValue(generator = "ESPACO_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private TipoContratacaoEnum tipoContratacao;
    private int quantidade;
    private int prazo;

    @ManyToOne
    @JoinColumn(name = "id_espaco", nullable = false)
    private EspacoEntity espaco;

    @ManyToOne
    @JoinColumn(name = "id_pacote")
    private PacoteEntity pacote;

    {
        quantidade = 0;
        prazo = 0;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public void setId(Integer integer) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

    @Override
    public String toString() {
        return "EspacosPacotesEntity{" +
                "id=" + id +
                ", espaco=" + espaco +
                ", pacote=" + pacote +
                ", tipoContratacao=" + tipoContratacao +
                ", quantidade=" + quantidade +
                ", prazo=" + prazo +
                '}';
    }

}
