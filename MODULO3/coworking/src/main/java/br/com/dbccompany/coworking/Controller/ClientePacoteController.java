package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.coworking.Service.ClientePacoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("api/cliente-pacote/")
public class ClientePacoteController {
    @Autowired
    private ClientePacoteService clientePacoteService;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "buscar/todos")
    @ResponseBody
    public List<ClientePacoteEntity> todos() {
        logger.info("requisicao (GET) api/cliente-pacote/todos " +
                "sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja cliente-pacote registrados");

        return clientePacoteService.todos();
    }

    @GetMapping(value = "/buscar/por-id/{id}")
    @ResponseBody
    public ClientePacoteEntity porId(@PathVariable int id) {
        logger.info("requisicao (GET) api/cliente-pacote/buscar/por-id/{id} " +
                "sendo processada, data: " + LocalDate.now());
        logger.info("id passado na requisicao: " + id);
        try {

            return Optional.ofNullable(
                    clientePacoteService.porId(id)
            ).get();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @GetMapping(value = "/buscar/por-cliente/{cpf}")
    @ResponseBody
    public List<ClientePacoteEntity> porId(@PathVariable String cpf) {
        logger.info("requisicao (GET) api/cliente-pacote/buscar/por-cliente/{cpf} " +
                "sendo processada, data: " + LocalDate.now());
        logger.info("CPF passado na requisicao: " + cpf);
        try {

            return Optional.ofNullable(
                    clientePacoteService.todosPorCliente(cpf)
            ).get();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @GetMapping(value = "/buscar/por-pacote/{id}")
    @ResponseBody
    public List<ClientePacoteEntity> porPacote(@PathVariable int id) {
        logger.info("requisicao (GET) api/cliente-pacote/buscar/por-pacote/{id} " +
                "sendo processada, data: " + LocalDate.now());
        logger.info("id passado na requisicao: " + id);
        try {

            return Optional.ofNullable(
                    clientePacoteService.todosPorPacote(id)
            ).get();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }
}
