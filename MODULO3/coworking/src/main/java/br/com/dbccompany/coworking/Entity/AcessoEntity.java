package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
public class AcessoEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ")
    @GeneratedValue(generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "ID_CLIENTE", nullable = false),
            @JoinColumn(name = "ID_ESPACO", nullable = false)
    })
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private SaldoClienteEntity saldoCliente;

    private boolean isEntrada;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDateTime dataDoAcesso;
    private boolean isExcecao;
    private long entryTime;
    private long departureTime;

    {
        this.isEntrada = false;
        this.isExcecao = false;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public LocalDate getDataDoAcesso() {
        return dataDoAcesso;
    }

    public void setDataDoAcesso(LocalDateTime dataDoAcesso) {
        if(dataDoAcesso == null) {
            dataDoAcesso = LocalDateTime.now();
        }
        this.dataDoAcesso = dataDoAcesso;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }

    public long getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(long entryTime) {
        this.entryTime = entryTime;
    }

    public long getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(long departureTime) {
        this.departureTime = departureTime;
    }
}