package br.com.dbccompany.coworking.DTO;

public class SaldoClienteDTO {

    private String cpfCliente;
    private String espacoNome;

    public SaldoClienteDTO() {
    }

    public SaldoClienteDTO(String cpfCliente, String espacoNome) {
        this.cpfCliente = cpfCliente;
        this.espacoNome = espacoNome;
    }

    public String getCpfCliente() {
        return cpfCliente;
    }

    public void setCpfCliente(String cpfCliente) {
        this.cpfCliente = cpfCliente;
    }

    public String getEspacoNome() {
        return espacoNome;
    }

    public void setEspacoNome(String espacoNome) {
        this.espacoNome = espacoNome;
    }
}
