package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Enum.TipoContratacaoEnum;

public class ContratacaoDTO {

    private TipoContratacaoEnum tipo;
    private String cpfCliente;
    private String espacoNome;
    private int quantidade;
    private int desconto;
    private int prazo;

    public ContratacaoDTO() {
    }

    public ContratacaoDTO(TipoContratacaoEnum tipo,
                          String cpfCliente, String espacoNome,
                          int quantidade, int desconto, int prazo) {
        this.tipo = tipo;
        this.cpfCliente = cpfCliente;
        this.espacoNome = espacoNome;
        this.quantidade = quantidade;
        this.desconto = desconto;
        this.prazo = prazo;
    }

    public TipoContratacaoEnum getTipo() {
        return tipo;
    }

    public void setTipo(TipoContratacaoEnum tipo) {
        this.tipo = tipo;
    }

    public String getCpfCliente() {
        return cpfCliente;
    }

    public void setCpfCliente(String cpfCliente) {
        this.cpfCliente = cpfCliente;
    }

    public String getEspacoNome() {
        return espacoNome;
    }

    public void setEspacoNome(String espacoNome) {
        this.espacoNome = espacoNome;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getDesconto() {
        return desconto;
    }

    public void setDesconto(int desconto) {
        this.desconto = desconto;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }
}
