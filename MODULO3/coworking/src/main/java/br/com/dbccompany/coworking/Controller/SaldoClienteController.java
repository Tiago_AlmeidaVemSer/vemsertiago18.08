package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.SaldoClienteDTO;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
@RequestMapping("api/saldo-cliente/")
public class SaldoClienteController {

    @Autowired
    private SaldoClienteService saldoClienteService;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "/buscar/todos")
    @ResponseBody
    public List<SaldoClienteEntity> todos() {
        logger.info("requisicao (GET) api/saldo-cliente/todos sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja saldo-cliente registrados");

        return saldoClienteService.todos();
    }

    @GetMapping(value = "/buscar/por-id")
    @ResponseBody
    public SaldoClienteEntity porId(@RequestBody SaldoClienteDTO dto) {
        logger.info("requisicao (GET) api/saldo-cliente/buscar/porid/{id} sendo processada, data: " + LocalDate.now());
        try {

            return Optional.ofNullable(
                    saldoClienteService.porId(dto)
            ).get();

        } catch (NoSuchElementException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }
}
