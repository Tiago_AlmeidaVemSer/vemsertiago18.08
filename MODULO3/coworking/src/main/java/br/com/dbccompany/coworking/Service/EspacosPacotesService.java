package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Repository.EspacoPacoteRepository;
import org.springframework.stereotype.Service;

@Service
public class EspacosPacotesService extends ServiceAbstract<EspacoPacoteRepository, EspacoPacoteEntity, Integer> {

}
