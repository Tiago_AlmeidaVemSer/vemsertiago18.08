package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.AcessoDTO;
import br.com.dbccompany.coworking.Entity.AcessoEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Repository.AcessoRepository;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AcessoService extends ServiceAbstract<AcessoRepository, AcessoEntity, Integer> {

    @Autowired
    SaldoClienteRepository saldoRepository;

    public AcessoDTO salvarEntrada(AcessoDTO acesso ){
        if( acesso.isEntrada() ) {
            SaldoClienteId id = acesso.getSaldoCliente().getId();
            SaldoClienteEntity saldo = saldoRepository.findById(id).get();
            //saldo.getVencimento().compareTo(new LocalDate());
            if(saldo.getQuantidade() <= 0) {
                return null;
            }
        }
        AcessoEntity acessoEntity = acesso.convert();
        AcessoDTO newDto = new AcessoDTO(super.salvar(acessoEntity));
        return newDto;
    }

}
