package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Ferramentas.ConversorValorStringXDouble;

public class EspacoDTO {

    private String nome;
    private int qtdPessoas;
    private String valor;

    public EspacoDTO() {
    }

    public EspacoDTO(String nome, int qtdPessoas, String valor) {
        this.nome = nome;
        this.qtdPessoas = qtdPessoas;
        this.valor = valor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public static EspacoEntity converterParaEntidade(EspacoDTO dto) {
        EspacoEntity entidade = new EspacoEntity();
        entidade.setNome(dto.nome);
        entidade.setQtdPessoas(dto.qtdPessoas);
        entidade.setValor(new ConversorValorStringXDouble(dto.valor).getValorEmDouble());
        return entidade;
    }

    public EspacoDTO converterParaDTO(EspacoEntity entidade) {
        EspacoDTO dto = new EspacoDTO();
        dto.setValor("R$ " + entidade.getValor());
        dto.setQtdPessoas(entidade.getQtdPessoas());
        dto.setNome(entidade.getNome());
        return dto;
    }

}
