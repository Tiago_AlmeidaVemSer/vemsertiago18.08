package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.Entity.EspacoEntity;import br.com.dbccompany.coworking.Service.EspacoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
@RequestMapping("api/espacos")
public class EspacoController {

    @Autowired
    private EspacoService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "buscar/todos")
    @ResponseBody
    public List<EspacoEntity> todos() {
        logger.info("requisicao (GET) api/espaco/todos " +
                "sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja contatos registrados");

        return service.todos();
    }

    @GetMapping(value = "/buscar/por-id/{id}")
    @ResponseBody
    public EspacoEntity porId(@PathVariable Integer id) {
        logger.info("requisicao (GET) api/contratacao/buscar/porid/{id} sendo processada, data: " + LocalDate.now());
        logger.info("id passado na requisicao: " + id);
        try {

            return service.porId(id);

        } catch (NoSuchElementException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @GetMapping(value = "/buscar/por-nome")
    @ResponseBody
    public EspacoEntity porNome(@RequestBody NomeEspacoDTO dto) {
        logger.info("requisicao (GET) api/contratacao/buscar/pornome/{nome} " +
                "sendo processada, data: " + LocalDate.now());
        logger.info("nome passado na requisicao: " + dto.getNome());
        try {

            return Optional.ofNullable(
                    service.buscarPorNome(dto.getNome())
            ).get();

        } catch (NoSuchElementException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @GetMapping(value = "/buscar/por-quantidade/igual/{quantidade}")
    @ResponseBody
    public EspacoEntity buscarPorQuantidadeDePessoas(@PathVariable int quantidade) {
        logger.info("requisicao (GET) api/contratacao/buscar/pornome/{nome} " +
                "sendo processada, data: " + LocalDate.now());
        logger.info("quantidade de pessoas passado na requisicao: " + quantidade);
        try {

            return Optional.ofNullable(
                    service.buscarPorQuantidadeDePessoas(quantidade)
            ).get();

        } catch (NoSuchElementException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @GetMapping(value = "/buscar/por-quantidade/igual-maior/{quantidade}")
    @ResponseBody
    public List<EspacoEntity> buscarPorQuantidadeDePessoasMaiorOuIgual(@PathVariable int quantidade) {
        logger.info("requisicao (GET) api/espaco/buscar/por-quantidade/igual-maior/{quantidade} " +
                "sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja espacos registrados que satisfacam a quantidade");

        return service.buscarPorQuantidadeDePessoasMaiorOuIgual(quantidade);
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public EspacoEntity salvar(@RequestBody EspacoDTO dto) {
        logger.info("requisicao (POST) api/espaco/novo sendo processada, data: " + LocalDate.now());
        try {

            return service.salvar(dto.converterParaEntidade(dto));

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public EspacoEntity editar(@PathVariable Integer id,@RequestBody EspacoDTO dto) {
        logger.info("requisicao (PUT) api/espaco/editar/{id} sendo processada, data: " + LocalDate.now());
        try {

            return service.editar(
                    EspacoDTO.converterParaEntidade(dto),
                    id
            );

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }
}
