package br.com.dbccompany.coworking.Entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SaldoClienteId implements Serializable {

    @Column(name = "ID_CLIENTE")
    private int id_cliente;

    @Column(name = "ID_ESPACO")
    private int id_espaco;

    public SaldoClienteId() {
    }

    public SaldoClienteId(int id_cliente, int id_espaco) {
        this.id_cliente = id_cliente;
        this.id_espaco = id_espaco;
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public int getId_espacos() {
        return id_espaco;
    }

    public void setId_espacos(int id_espaco) {
        this.id_espaco = id_espaco;
    }
}
