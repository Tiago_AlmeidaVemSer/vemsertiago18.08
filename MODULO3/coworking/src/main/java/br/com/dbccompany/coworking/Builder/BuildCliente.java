package br.com.dbccompany.coworking.Builder;

import br.com.dbccompany.cowork.DTO.ClienteDTO;
import br.com.dbccompany.cowork.Entity.ClienteEntity;

public class BuildCliente {
    public static ClienteEntity entidade(ClienteDTO dto) {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome(dto.getNome());
        cliente.setDataNascimento(dto.getDataNascimento());
        cliente.setCpf(dto.getCpf());
        return cliente;
    }

    public static  ClienteDTO DTO(ClienteEntity cliente) {
        ClienteDTO dto = new ClienteDTO();
        dto.setCpf(cliente.getCpf());
        dto.setNome(cliente.getNome());
        dto.setCpf(cliente.getCpf());
        dto.setDataNascimento(cliente.getDataNascimento());
        return dto;
    }
}
