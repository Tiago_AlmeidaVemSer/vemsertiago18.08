package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.Entity.EntityAbstract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public abstract class ServiceAbstract<
        R extends CrudRepository<E, T>,
        E extends EntityAbstract, T> {

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    protected R repository;

    @Transactional(rollbackFor = Exception.class)
    public E salvar(E entidade) {
        logger.warn("Se estiver campos faltando não irá salvar a Entidade");
        try{
            return repository.save(entidade);
        }catch(Exception e){
            logger.error("Erro ao tentar salvar Entidade " + e.getMessage());
            return null;
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public E editar(E entidade, T id) {
        logger.warn("se o id for invalido nao vai editar Entidade");
        try{
            entidade.setId(id);
            return repository.save(entidade);
        }catch(Exception e){
            logger.error("Erro ao tentar editar Entidade");
            return null;
        }

    }

    public List<E> todos(){
        logger.info("carregando lista");
        return (List<E>) repository.findAll();
    }

    public E porId(T id){
        logger.warn("se o id for invalido nao busca");
        try{
            return repository.findById(id).get();
        }catch(Exception e){
            logger.error("erro ao buscar por id");
            return null;
        }

    }

}
