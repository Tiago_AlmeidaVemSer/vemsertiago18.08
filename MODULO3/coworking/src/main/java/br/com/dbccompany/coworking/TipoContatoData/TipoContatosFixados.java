package br.com.dbccompany.coworking.TipoContatoData;

import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TipoContatosFixados {

    @Autowired
    private TipoContatoRepository repository;

    @PostConstruct
    public void tipoContatosBasicos(){

        TipoContatoEntity email = new TipoContatoEntity();
        email.setNome("email");

        TipoContatoEntity telefone = new TipoContatoEntity();
        telefone.setNome("telefone");

        TipoContatoEntity whatsapp = new TipoContatoEntity();
        whatsapp.setNome("whatsapp");

        List<TipoContatoEntity> list = new ArrayList<>(Arrays.asList(email, telefone, whatsapp));

        for(TipoContatoEntity tipo : list){
            repository.save(tipo);
            System.out.println(tipo.getId());
            System.out.println(tipo.getNome());
        }
    }
}
