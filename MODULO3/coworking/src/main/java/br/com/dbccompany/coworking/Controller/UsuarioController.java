package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.Service.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
@RequestMapping("api/usuario/")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<UsuarioEntity> todos() {
        logger.info("requisicao (GET) api/usuario/todos sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja usuarios registrados");

        return usuarioService.todos();
    }

    @GetMapping(value = "buscar/por-id/{id}")
    @ResponseBody
    public UsuarioEntity porId(@PathVariable("id") int id) {
        logger.info("requisicao (GET) api/usuario/buscar/porid/{id} sendo processada, data: " + LocalDate.now());
        logger.info("id passado na requisicao: " + id);
        try {

            return Optional.ofNullable(
                    usuarioService.porId(id)
            ).get();

        } catch (NoSuchElementException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }


    @GetMapping(value = "buscar/por-email/{email}")
    @ResponseBody
    public UsuarioEntity porEmail(@PathVariable String email) {
        logger.info("requisicao (GET) api/usuario/buscar/por-email/{email} sendo processada, data: " + LocalDate.now());
        logger.info("email passado na requisicao: " + email);
        try {

            return Optional.ofNullable(
                    usuarioService.porEmail(email)
            ).get();

        } catch (NoSuchElementException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @GetMapping(value = "buscar/por-login/{login}")
    @ResponseBody
    public UsuarioEntity porLogin(@PathVariable String login) {
        logger.info("requisicao (GET) api/usuario/buscar/porid/{id} sendo processada, data: " + LocalDate.now());
        logger.info("login passado na requisicao: " + login);
        try {

            return Optional.ofNullable(
                    usuarioService.porLogin(login)
            ).get();

        } catch (NoSuchElementException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public UsuarioEntity novo(@RequestBody UsuarioEntity usuario) {
        logger.info("requisicao (POST) api/usuario/novo sendo processada, data: " + LocalDate.now());
        try {

            return usuarioService.salvar(usuario);

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public UsuarioEntity editar(@PathVariable int id, @RequestBody UsuarioEntity usuario) {
        logger.info("requisicao (PUT) api/usuario/editar/{id} sendo processada, data: " + LocalDate.now());
        try {

            return usuarioService.editar(usuario, id);

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }
}