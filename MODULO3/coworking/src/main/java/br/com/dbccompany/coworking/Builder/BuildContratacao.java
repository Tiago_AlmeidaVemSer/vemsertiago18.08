package br.com.dbccompany.coworking.Builder;

import br.com.dbccompany.cowork.DTO.ContratacaoDTO;
import br.com.dbccompany.cowork.Entity.ContratacaoEntity;

public class BuildContratacao {

    public static ContratacaoEntity entidade(ContratacaoDTO dto) {
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setTipoContratacao(dto.getTipo());
        contratacao.setQuantidade(dto.getQuantidade());
        contratacao.setDesconto(dto.getDesconto());
        contratacao.setPrazo(dto.getPrazo());
        return contratacao;
    }
}