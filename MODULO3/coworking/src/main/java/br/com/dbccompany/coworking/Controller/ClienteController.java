package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Service.ClienteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
@RequestMapping("api/cliente")
public class ClienteController {

    @Autowired
    private ClienteService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ClienteDTO> todos() {
        logger.info("requisicao (GET) api/cliente/todos sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja clientes registrados");
        return service.todosClientes();
    }

    @GetMapping(value = "/buscar/por-cpf/{cpf}")
    @ResponseBody
    public ClienteEntity porCpf(@PathVariable String cpf) {
        logger.info("requisicao (GET) api/cliente/buscar/porcpf/{cpf} sendo processada, data: " + LocalDate.now());
        logger.info("cpf passado na requisicao: " + cpf);
        try {
            Optional<ClienteEntity> cliente = Optional.ofNullable(service.buscarPorCpf(cpf));
            return cliente.get();
        } catch (NoSuchElementException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ClienteEntity salvar(@RequestBody ClienteDTO dto) {
        logger.info("requisicao (POST) api/cliente/novo sendo processada, data: " + LocalDate.now());
        try {
            ClienteEntity cliente = service.salvar(dto);
            if (cliente == null) {
                logger.error("Dados nao encontrados");
            } else {
                logger.info("requisicao realizada com sucesso \n" + cliente.toString());
            }
            return cliente;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ClienteEntity editar(@PathVariable int id, @RequestBody ClienteDTO dto) {
        logger.info("requisicao (PUT) api/cliente/editar sendo processada, data: " + LocalDate.now());
        try {
            ClienteEntity cliente = service.editar(dto, id);
            logger.info("requisicao realizada com sucesso \n" + cliente.toString());
            return cliente;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }
}
