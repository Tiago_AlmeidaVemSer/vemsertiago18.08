package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import org.springframework.stereotype.Service;

@Service
public class EspacoService extends ServiceAbstract<EspacoRepository, EspacoEntity, Integer> {

}
