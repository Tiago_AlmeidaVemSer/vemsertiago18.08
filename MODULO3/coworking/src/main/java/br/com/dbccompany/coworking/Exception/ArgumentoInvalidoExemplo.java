package br.com.dbccompany.coworking.Exception;

public class ArgumentoInvalidoExemplo extends ExceptionExemplo {

    public ArgumentoInvalidoExemplo() {
        super("Faltou argumento para o exemplo");
    }
}
