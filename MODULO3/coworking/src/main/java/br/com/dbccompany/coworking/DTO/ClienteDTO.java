package br.com.dbccompany.coworking.DTO;

import java.time.LocalDate;

public class ClienteDTO {

    private String nome;
    private String cpf;
    private LocalDate dataNascimento;
    private String emailValor;
    private String telefoneValor;

    public ClienteDTO() {
    }

    public ClienteDTO(String nome, String cpf, LocalDate dataNascimento, String email, String telefone) {
        this.nome = nome;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
        this.emailValor = email;
        this.telefoneValor = telefone;
    }

    public ClienteDTO(String nome, String cpf, String email, String telefone) {
        this.nome = nome;
        this.cpf = cpf;
        this.emailValor = email;
        this.telefoneValor = telefone;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getEmailValor() {
        return emailValor;
    }

    public void setEmailValor(String emailValor) {
        this.emailValor = emailValor;
    }

    public String getTelefoneValor() {
        return telefoneValor;
    }

    public void setTelefoneValor(String telefoneValor) {
        this.telefoneValor = telefoneValor;
    }
}
