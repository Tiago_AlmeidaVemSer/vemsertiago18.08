package br.com.dbccompany.coworking.Exception;

public class ExceptionExemplo extends Exception{

    private String mensagem;

    public ExceptionExemplo(String mensagem) {
        super(mensagem);
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
