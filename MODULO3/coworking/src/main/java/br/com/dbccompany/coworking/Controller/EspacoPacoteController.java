package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.Entity.EspacoPacoteEntity;
import br.com.dbccompany.coworking.Service.EspacosPacotesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("api/espacopacotes")
public class EspacoPacoteController {

    @Autowired
    private EspacosPacotesService espacosPacotesService;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "todos")
    @ResponseBody
    public List<EspacoPacoteEntity> todos() {
        logger.info("requisicao (GET) api/espaco-pacote/todos " +
                "sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja " +
                "espaco-pacote registrados");

        return espacosPacotesService.todos();
    }
