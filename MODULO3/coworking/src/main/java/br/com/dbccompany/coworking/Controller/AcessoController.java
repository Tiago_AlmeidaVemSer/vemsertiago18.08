package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.AcessoDTO;
import br.com.dbccompany.coworking.Entity.AcessoEntity;
import br.com.dbccompany.coworking.Service.AcessoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
@RequestMapping("/api/acesso")
public class AcessoController {

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    private AcessoService service;

    @PostMapping(value = "/salvar/entrada")
    @ResponseBody
    public ResponseEntity<String> salvarEntrada(@RequestBody AcessoDTO dto) {
        logger.info("requisicao (POST) api/acesso/salvar sendo processada, data: " + LocalDate.now());
        try {
            service.salvarEntrada(dto);
            logger.info("processo terminado com sucesso");
            return ResponseEntity.ok().body("Acesso de entrada salvo com sucesso! Horario: " + LocalDate.now());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping(value = "/salvar/saida")
    @ResponseBody
    public ResponseEntity<String> salvarSaida(@RequestBody AcessoDTO dto) {
        logger.info("requisicao (POST) api/acesso/novo/saida sendo processada, data: " + LocalDate.now());
        try {
            service.salvarSaida(dto);
            logger.info("processo terminado com sucesso");
            return ResponseEntity.ok().body("Acesso de entrada salvo com sucesso! Horario: " + LocalDate.now());

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<AcessoEntity> todos() {
        logger.info("requisicao (GET) api/acesso/todos sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja acessos registrados");
        return service.todos();
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<String> editar(@PathVariable int id, @RequestBody AcessoEntity acessoEntity) {
        logger.info("requisicao (PUT) api/acesso/editar sendo processada, data: " + LocalDate.now());
        try {
            acessoEntity.setId(id);
            service.salvar(acessoEntity);
            logger.info("processo terminado com sucesso");
            return ResponseEntity.ok().body("Atualizacao realizada com sucesso");

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return ResponseEntity.badRequest().body("Erro ao processar a atualizacao");
    }

    @GetMapping(value = "/buscar/porid/{id}")
    @ResponseBody
    public AcessoEntity porId(@PathVariable int id) {
        logger.info("requisicao (GET) api/acesso/buscar/porid/{id} sendo processada, data: " + LocalDate.now());
        logger.info("id passado na requisicao: " + id);
        try {
            Optional<AcessoEntity> acesso = Optional.ofNullable(service.porId(id));
            return acesso.get();
        } catch (NoSuchElementException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }
}
