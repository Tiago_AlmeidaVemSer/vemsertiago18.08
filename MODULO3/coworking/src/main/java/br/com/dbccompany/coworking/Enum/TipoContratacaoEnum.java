package br.com.dbccompany.coworking.Enum;

public enum TipoContratacaoEnum {
    MINUTOS, HORAS, TURNOS, DIARIAS, SEMANAS, MESES
}
