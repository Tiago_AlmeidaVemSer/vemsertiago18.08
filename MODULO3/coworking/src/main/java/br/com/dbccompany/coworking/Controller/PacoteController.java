package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Service.PacoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
@RequestMapping("api/pacotes")
public class PacoteController {

    @Autowired
    private PacoteService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @PostMapping(value = "/novo")
    @ResponseBody
    public String salvar(@RequestBody PacoteDTO dto) {
        logger.info("requisicao (POST) api/pacote/novo sendo processada, data: " + LocalDate.now());
        try {

            return "R$ " + service.salvar(dto);

        } catch (Exception e) {
            logger.error(e.getMessage());
            return "Pacote nao pode ser salvo";
        }
    }

    @GetMapping(value = "buscar/todos")
    @ResponseBody
    public List<PacoteEntity> todos() {
        logger.info("requisicao (GET) api/pacote/todos sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja pacotes registrados");

        return service.todos();
    }

    @GetMapping(value = "/buscar/por-id/{id}")
    @ResponseBody
    public PacoteEntity buscarPorId(@PathVariable int id) {
        logger.info("requisicao (GET) api/pacote/buscar/porid/{id} sendo processada, data: " + LocalDate.now());
        logger.info("id passado na requisicao: " + id);
        try {

            return Optional.ofNullable(
                    service.porId(id)
            ).get();

        } catch (NoSuchElementException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }
}
