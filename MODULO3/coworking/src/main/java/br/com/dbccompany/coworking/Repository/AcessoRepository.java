package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.AcessoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AcessoRepository extends CrudRepository<AcessoEntity, Integer> {

}
