package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class ClienteRepositoryTest {

    @Autowired
    private ClienteRepository repository;

    private ClienteEntity cliente;
    private Date data;

    @BeforeEach
    public void setup() {
        data = new Date(System.currentTimeMillis());
        cliente = new ClienteEntity();
        cliente.setCpf("10114460060");
        cliente.setNome("NomeTeste");
        cliente.setDataNascimento(data);
    }

    @Test
    public void buscarPorCPF() {
        repository.save(cliente);
        ClienteEntity clientePersistido = repository.findByCpf("10114460060");
        assertEquals(cliente.getNome(), clientePersistido.getNome());
        assertEquals(cliente.getCpf(), clientePersistido.getCpf());
        assertEquals(cliente.getDataNascimento(), clientePersistido.getDataNascimento());
    }

    @Test
    public void nullBuscarPorCPFNaoRegistrado() {
        ClienteEntity clientePersistido = repository.findByCpf("10114460060");
        assertNull(clientePersistido);

    }

    @Test
    public void buscarPorNome() {
        repository.save(cliente);
        ClienteEntity clientePersistido = repository.findByNome("NomeTeste");
        assertEquals(cliente.getNome(), clientePersistido.getNome());
        assertEquals(cliente.getCpf(), clientePersistido.getCpf());
        assertEquals(cliente.getDataNascimento(), clientePersistido.getDataNascimento());
    }

    @Test
    public void nullBuscarPorNomeNaoRegistrado() {
        ClienteEntity clientePersistido = repository.findByNome("NomeTeste");
        assertNull(clientePersistido);
    }
}
