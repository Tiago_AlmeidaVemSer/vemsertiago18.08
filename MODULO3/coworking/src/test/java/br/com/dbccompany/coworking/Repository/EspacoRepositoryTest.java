package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class EspacoRepositoryTest {

    @Autowired
    private EspacoRepository repository;

    private EspacoEntity espaco;

    @BeforeEach
    public void setup() {
        espaco = new EspacoEntity();
        espaco.setNome("Sala 01 - Reuniao Executiva");
        espaco.setQtdPessoas(10);
        espaco.setValor(450.0);
    }

    @Test
    public void buscarPorNome() {
        repository.save(espaco);
        EspacoEntity espacoPersistido = repository.findByNome("Sala 01 - Reuniao Executiva");
        assertEquals(espaco.getNome(), espacoPersistido.getNome());
        assertEquals(espaco.getQtdPessoas(), espacoPersistido.getQtdPessoas());
    }

    @Test
    public void nullBuscarPorNomeNaoRegistrado() {
        EspacoEntity espacoPersistido = repository.findByNome("Sala 01 - Reuniao Executiva");
        assertNull(espacoPersistido);
    }

    @Test
    public void buscarPorQuantidadeDePessoas() {
        repository.save(espaco);
        EspacoEntity espacoPersistido = repository.findByQtdPessoas(10);
        assertEquals(espaco.getNome(), espacoPersistido.getNome());
    }

    @Test
    public void nullBuscarPorQuantidadeDePessoasNaoRegistrado() {
        EspacoEntity espacoPersistido = repository.findByQtdPessoas(10);
        assertNull(espacoPersistido);
    }

}
