package br.com.dbccompany.vemser.Entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Entity
@Inheritance( strategy = InheritanceType.JOINED)
public class UsuarioEntity extends EntityAbstract<Integer> implements Serializable, UserDetails {

    @Id
    @SequenceGenerator(name = "USUARIO_SEQ", sequenceName = "USUARIO_SEQ")
    @GeneratedValue(generator = "USUARIO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String nome;
    @Column(length = 11, columnDefinition = "CHAR")
    private String cpf;

    private String login;
    private String senha;


    private char dataNascimento;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_ENDERECO")
    private EnderecoEntity endereco;

    @Enumerated(EnumType.STRING)
    private EstatadoCivilEnum estatadoCivil;

    @OneToMany(mappedBy = "usuario")
    private List<ContaClienteEntity> contasClientes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public char getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(char dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public EnderecoEntity getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoEntity endereco) {
        this.endereco = endereco;
    }

    public EstatadoCivilEnum getEstatadoCivil() {
        return estatadoCivil;
    }

    public void setEstatadoCivil(EstatadoCivilEnum estatadoCivil) {
        this.estatadoCivil = estatadoCivil;
    }

    public List<ContaClienteEntity> getContasClientes() {
        return contasClientes;
    }

    public void setContasClientes(List<ContaClienteEntity> contasClientes) {
        this.contasClientes = contasClientes;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}
