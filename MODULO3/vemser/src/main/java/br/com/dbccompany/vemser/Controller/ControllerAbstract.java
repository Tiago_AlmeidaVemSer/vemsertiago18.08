package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.EntityAbstract;
import br.com.dbccompany.vemser.Service.ServiceAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class ControllerAbstract<
        S extends ServiceAbstract,
        E extends EntityAbstract, I> {

    @Autowired
    S service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<E> todosPais(){
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public E salvar(@RequestBody E entidade){
        return (E) service.salvar(entidade);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public E especifico(@PathVariable I id){
        return (E) service.porId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public E editar(@PathVariable I id, @RequestBody E entidade){
        return (E) service.editar(entidade,id);
    }

}
