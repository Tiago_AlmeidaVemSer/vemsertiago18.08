package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.TipoContaEntity;
import br.com.dbccompany.vemser.Repository.TipoContaRepository;
import org.springframework.stereotype.Service;

@Service
public class TipoContaService extends ServiceAbstract<TipoContaRepository, TipoContaEntity, Integer>{

}