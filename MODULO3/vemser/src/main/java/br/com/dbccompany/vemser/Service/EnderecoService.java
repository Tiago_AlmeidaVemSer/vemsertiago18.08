package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import br.com.dbccompany.vemser.Entity.EstadoEntity;
import br.com.dbccompany.vemser.Repository.EnderecoRepository;
import br.com.dbccompany.vemser.Repository.EstadoRepository;
import org.springframework.stereotype.Service;

@Service
public class EnderecoService extends ServiceAbstract<EnderecoRepository, EnderecoEntity, Integer>{

}
