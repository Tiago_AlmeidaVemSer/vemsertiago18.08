package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import br.com.dbccompany.vemser.Entity.MovimentacoesEntity;
import br.com.dbccompany.vemser.Repository.AgenciaRepository;
import br.com.dbccompany.vemser.Repository.MovimentacoesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MovimentacoesService extends ServiceAbstract<MovimentacoesRepository, MovimentacoesEntity, Integer> {

}
