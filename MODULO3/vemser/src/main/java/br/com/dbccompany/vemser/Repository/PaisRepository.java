package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.PaisEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaisRepository extends CrudRepository<PaisEntity, Integer> {
    PaisEntity findByNome( String nome );
}
