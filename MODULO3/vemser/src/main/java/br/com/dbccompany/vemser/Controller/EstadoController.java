package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.EstadoEntity;
import br.com.dbccompany.vemser.Entity.PaisEntity;
import br.com.dbccompany.vemser.Service.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/estado" )
public class EstadoController {

    @Autowired
    EstadoService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<EstadoEntity> todosEstados() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public EstadoEntity salvar(@RequestBody EstadoEntity estado){
        return service.salvar(estado);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public EstadoEntity estadoEspecifico(@PathVariable Integer id){
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public EstadoEntity editarEstado(@PathVariable Integer id, @RequestBody EstadoEntity estado){
        return service.editar(estado, id);
    }
}
