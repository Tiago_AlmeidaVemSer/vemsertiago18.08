import java.util.ArrayList;

public class EstatisticasInventario{    
    private Inventario inventario;
    
    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    public double calcularMedia(){
        if(this.inventario.getItens().isEmpty()){
            return Double.NaN;
        }
        double quantidade = .0;
        for(Item item : inventario.getItens()){
            quantidade += item.getQuantidade();
        }
        double media = quantidade / inventario.getItens().size();
        return media;
    }
    
    public double calcularMediana(){
        if(inventario.getItens().isEmpty()){
            return Double.NaN;
        }
        int qtdItens = this.inventario.getItens().size();
        int meio = qtdItens / 2;
        int qtdMeio = this.inventario.obter(meio).getQuantidade();
        boolean qtdImpar = qtdItens % 2 == 1;
        if(qtdImpar){
            return qtdMeio;
        }
        
        int qtdMeioMenosUm = this.inventario.obter(meio - 1).getQuantidade();
        return (qtdMeio + qtdMeioMenosUm) / 2.0;        
    }
    
    public int qtdItensAcimaDaMedia(){        
        int quantidade = 0;
        for(Item item : inventario.getItens()){
            if(item.getQuantidade() > this.calcularMedia()){
                quantidade++;
            }
        }
        return quantidade;
    }
}
