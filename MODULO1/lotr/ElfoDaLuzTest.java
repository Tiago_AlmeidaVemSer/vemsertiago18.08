import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest {
    
    @Test
    public void atacarImparPerde21DeVida(){
        ElfoDaLuz luz = new ElfoDaLuz("Iluminado");
        luz.atacarComEspada(new Dwarf("zeca"));
        
        assertEquals(79.0, luz.getVida(), 1e-8);        
    }
    
    @Test
    public void atacarParPerde10DeVida(){
        ElfoDaLuz luz = new ElfoDaLuz("Iluminado");
        luz.atacarComEspada(new Dwarf("zeca"));
        luz.atacarComEspada(new Dwarf("zeca"));
        
        assertEquals(89.0, luz.getVida(), 1e-8);     
    }
    
    @Test
    public void atacarGanha1Xp(){
        ElfoDaLuz luz = new ElfoDaLuz("Iluminado");
        luz.atacarComEspada(new Dwarf("zeca"));
        
        assertEquals(1, luz.getExperiencia());        
    }
}
