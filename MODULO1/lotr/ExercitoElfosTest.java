import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ExercitoElfosTest {
    
    @Test
    public void podeAlistarElfoVerde(){
        Elfo elfoVerde = new ElfoVerde("Green Legolas");              
        ExercitoElfos exercito = new ExercitoElfos();
        exercito.alistar(elfoVerde);     
        assertTrue(exercito.getElfos().contains(elfoVerde));
    }
    
    @Test
    public void podeAlistarElfoNoturno(){
        Elfo elfoNoturno = new ElfoNoturno("Dark Legolas");              
        ExercitoElfos exercito = new ExercitoElfos();
        exercito.alistar(elfoNoturno);     
        assertTrue(exercito.getElfos().contains(elfoNoturno));
    }
    
    @Test
    public void naoPodeAlistarElfoDaLuz(){
        Elfo elfoDaLuz = new ElfoDaLuz("Light Legolas");              
        ExercitoElfos exercito = new ExercitoElfos();
        exercito.alistar(elfoDaLuz);     
        assertFalse(exercito.getElfos().contains(elfoDaLuz));
    }
    
     @Test
    public void buscarElfosRecemCriadosExistindo(){
        Elfo elfoNoturno = new ElfoNoturno("Night Legolas");
        ExercitoElfos exercito = new ExercitoElfos();
        exercito.alistar(elfoNoturno);
        ArrayList<Elfo> esperado = new ArrayList<>(
            Arrays.asList(elfoNoturno)
        );
        assertEquals(esperado, exercito.buscar(Status.RECEM_CRIADO));
    }
    
}
