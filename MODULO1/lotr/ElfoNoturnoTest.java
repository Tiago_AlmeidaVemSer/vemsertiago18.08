import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest {
    
    @Test
    public void ganhaTriploExperienciaAoAtirarUmaFlecha(){
        ElfoNoturno ernodo = new ElfoNoturno("Ernodo");
        Dwarf thurin = new Dwarf("Thurin");
        ernodo.atirarFlecha(thurin);
        Inventario inventario = ernodo.getInventario();
        assertEquals(1, ernodo.inventario.obter(1).getQuantidade());
        assertEquals(3, ernodo.getExperiencia());
    }
    
    @Test
    public void perde15DeVidaAoAtirarUmaFlecha(){
        ElfoNoturno ernodo = new ElfoNoturno("Ernodo");
        Dwarf thurin = new Dwarf("Thurin");
        ernodo.atirarFlecha(thurin);
        
        assertEquals(85.0, ernodo.getVida(), 1e-9);
    }
    
    @Test
    public void perde30DeVidaAoAtirarDuasFlechasGanhaTriploExperiencia(){
        ElfoNoturno ernodo = new ElfoNoturno("Ernodo");
        Dwarf thurin = new Dwarf("Thurin");
        ernodo.atirarFlecha(thurin);
        ernodo.atirarFlecha(thurin);
        
        assertEquals(6, ernodo.getExperiencia());
        assertEquals(70.0, ernodo.getVida(), 1e-9);
    }
    
    @Test
    public void elfoNoturnoAtira7FlechasEMorre(){
        ElfoNoturno noturno = new ElfoNoturno("Noturno");
        noturno.getInventario().obter(1).setQuantidade(1000);
        
        noturno.atirarFlecha(new Dwarf("Balin"));
        noturno.atirarFlecha(new Dwarf("Balin"));
        noturno.atirarFlecha(new Dwarf("Balin"));
        noturno.atirarFlecha(new Dwarf("Balin"));
        noturno.atirarFlecha(new Dwarf("Balin"));
        noturno.atirarFlecha(new Dwarf("Balin"));
        noturno.atirarFlecha(new Dwarf("Balin"));
        
        assertEquals( .0, noturno.getVida(), 1e-9 );
        assertEquals( Status.MORTO, noturno.getStatus() );
    }
}
