import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest{
    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void elfoDeveNascerCom2Flechas(){
        Elfo elfoQualquer = new Elfo("Legolas");
        assertEquals(2, elfoQualquer.getQtdFlechas());
    }
    
    @Test
    public void atirarFlechaDevePerderFlechaEAumentarXP(){
        Elfo elfo = new Elfo("Legolas");
        Dwarf anao = new Dwarf("Thorin");
         
        elfo.atirarFlecha(anao);          
        assertEquals(1, elfo.getQtdFlechas());
        assertEquals(1, elfo.getExperiencia());
    }
    
    @Test
    public void atirar3FlechaDevePerderFlechaEAumentarXP(){
        Elfo elfoEspecifico = new Elfo("Legolas");
        Dwarf duarf = new Dwarf("Thorin");
        //Acao
        elfoEspecifico.atirarFlecha(new Dwarf("outroDwarf"));
        elfoEspecifico.atirarFlecha(new Dwarf("maisUmDwarf"));   
        elfoEspecifico.atirarFlecha(duarf);  
        //verificacao
        assertEquals(0, elfoEspecifico.getQtdFlechas());
        assertEquals(2, elfoEspecifico.getExperiencia());
    }
    
    @Test
    public void atirarFlechaDeveAtirar3FlechasTendoSomente2(){
        Elfo elfoEspecifico = new Elfo("Legolas");
        Dwarf duarf = new Dwarf("Thorin");
        elfoEspecifico.atirarFlecha(duarf);
        elfoEspecifico.atirarFlecha(duarf); 
        elfoEspecifico.atirarFlecha(duarf);
        assertEquals(0, elfoEspecifico.getQtdFlechas());
    }
    
    @Test
    public void atirarFlechaDeveAtirarFlechaSomenteEmDwarf(){
        Dwarf dwarf = new Dwarf("Gimli");
        Elfo elfo = new Elfo("Aredhel");
        elfo.atirarFlecha(dwarf);        
        assertEquals(100.0, dwarf.getVida(),0.001);
    }
      
    
    @Test
    public void atirarFlechaEmDwarfTiraVida(){
        Dwarf dwarf = new Dwarf("Gimli");
        Elfo elfo = new Elfo("Aredhel");
        //acao
        elfo.atirarFlecha(dwarf);
        //verificacao
        assertEquals(1, elfo.getQtdFlechas());
        assertEquals(1,elfo.getExperiencia());
        assertEquals(100.0, dwarf.getVida(),0.001);
    }
    
    @Test
    public void criarUmElfoIncrementaContadorUmaVez(){
        Elfo elfo = new Elfo("elfo");        
        assertEquals(1, elfo.getQtdElfos());
    }
    
    @Test
    public void criarDoisElfoIncrementaContadorUmaVez(){
        Elfo elfo = new Elfo("elfo");
        Elfo elfo1 = new Elfo("elfo");
        assertEquals(2, elfo.getQtdElfos());
    }
}
