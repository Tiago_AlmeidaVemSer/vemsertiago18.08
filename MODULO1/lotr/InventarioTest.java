
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class InventarioTest
{
    @Test
    public void adicionarUmItem() {
        Inventario inventario = new Inventario(1);
        Item item = new Item(1, "Espada");
        inventario.adicionar(item);
        assertEquals(item, inventario.getItens().get(0));
    }
    
     @Test
    public void deveAdicionarDoisItens(){
        Inventario inventario = new Inventario(1);
        Item arco = new Item(1,"arco");
        Item escudo = new Item(1,"escudo");
        inventario.adicionar(arco);
        inventario.adicionar(escudo);
        assertEquals(arco,inventario.obter(0));
        assertEquals(escudo,inventario.obter(1));
    }
    
    @Test
    public void deveAdicionarUmItemEObter(){
        Inventario inventario = new Inventario(1);
        Item item = new Item(1,"flecha");
        inventario.adicionar(item);
        assertEquals(item,inventario.obter(0));        
    }     
       
    @Test
    public void adicionarUmItemERemover(){
        Inventario inventario = new Inventario(1);
        Item item = new Item(1,"arco");        
        inventario.adicionar(item);
        inventario.remover(0);
        assertTrue(!inventario.getItens().contains(item));
    }
    
    @Test
    public void getDescricoesVariosItens() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        String resultado = inventario.getDescricoesItens();
        assertEquals( "Espada,Escudo", resultado );
    }
    
    @Test
    public void deveRetornarItemComMaiorQuantidadeNoArray(){
        Inventario inventario = new Inventario(1);
        
        Item arco = new Item(1,"arco");
        Item flecha = new Item(3,"flecha");
        Item machado = new Item(2,"machado");              
        Item capa = new Item(8,"capa");
        Item escudo = new Item(3,"escudo");
        Item espada = new Item(1,"espada");
        
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.adicionar(machado);              
        inventario.adicionar(capa);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        
        Item resultado = inventario.maiorQtd();
        
        assertEquals(capa, resultado);
    }
    
     @Test
    public void deveRetornarItemMaiorQuantidadeComMesmaQuantidade() {
        Inventario inventario = new Inventario(1);
        Item lanca = new Item(5,"Lança");
        Item espada = new Item(5, "Espada");
        Item escudo = new Item(2, "Escudo");
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        Item resultado = inventario.maiorQtd();
        assertEquals(lanca, resultado);
    }
    
    @Test
    public void buscarApenasUmItem() {
        Inventario inventario = new Inventario(1);
        Item cafe = new Item(1, "cafe");
        inventario.adicionar(cafe);
        Item resultado = inventario.buscar(new String("cafe"));
        assertEquals(cafe, resultado);
    }
    
    @Test
    public void inverterDoisItens() {
        Inventario inventario = new Inventario(2);
        Item termica = new Item(1, "Termica de Café");
        Item energetico = new Item(1, "Energetico");
        inventario.adicionar(termica);
        inventario.adicionar(energetico);
        ArrayList<Item> resultado = inventario.inverter();
        
        assertEquals(energetico, resultado.get(0));
        assertEquals(termica, resultado.get(1));
        
        assertEquals(termica, inventario.obter(0));
        assertEquals(energetico, inventario.obter(1));
        
        assertEquals(2, resultado.size());
    }
    
    @Test
    public void deveOrdenarOsItensDoInventarioPorQuantidadeDeFormaAscendente(){
        Inventario inventario = new Inventario(1);        
        ArrayList<Item> ordenados = new ArrayList<>();
        ArrayList<Item> resultado = new ArrayList<>();

        Item espada = new Item(2, "espada");
        Item escudo = new Item(1, "escudo");
        Item arco = new Item(3, "arco");        
        
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(arco);
        
        ordenados.add(escudo);
        ordenados.add(espada);
        ordenados.add(arco);
        
        inventario.ordenarItens(TipoOrdenacao.ASC);
        resultado = inventario.getItens();
        assertEquals(ordenados, resultado);
    }
    
    @Test
    public void deveOrdenarOsItensDoInventarioPorQuantidadeDeFormaDescendente(){
        Inventario inventario = new Inventario(1);        
        ArrayList<Item> ordenados = new ArrayList<>();
        ArrayList<Item> resultado = new ArrayList<>();

        Item espada = new Item(2, "espada");
        Item escudo = new Item(1, "escudo");
        Item arco = new Item(3, "arco");        
        
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(arco);
        
        ordenados.add(arco);
        ordenados.add(espada);
        ordenados.add(escudo);
        
        inventario.ordenarItens(TipoOrdenacao.DESC);
        resultado = inventario.getItens();
        assertEquals(ordenados, resultado);
    }
}
