import java.util.*;

public class ComparadorDePerformance {
    
    public void comparar() {
        ArrayList<Elfo> arrayElfo = new ArrayList<>();
        HashMap<String, Elfo> hashMapElfos = new HashMap<>();
        int qtdElfos = 150000;
        for(int i = 0; i < qtdElfos; i++){
            String nome = "Elfo " + i;
            Elfo elfo = new Elfo( nome );
            arrayElfo.add( elfo );
            hashMapElfos.put(nome, elfo);
        }
        
        String nomeBusca = "Elfo 100000";
        
        long mSeqInicio = System.currentTimeMillis();
        Elfo elfoSeq = buscarSequencial(arrayElfo, nomeBusca);
        long mSeqFim = System.currentTimeMillis();
        
        long mMapInicio = System.currentTimeMillis();
        Elfo elfoMap = buscarMap(hashMapElfos, nomeBusca);
        long mMapFim = System.currentTimeMillis();
        
        String tempoSeq = String.format("%.10f",( mSeqFim - mSeqInicio ));
        String tempoMap = String.format("%.10f",( mMapFim - mMapInicio ));
        
        System.out.println("ArrayList: " + tempoSeq);
        System.out.println("HashMap: " + tempoMap);
    }
    
    private Elfo buscarSequencial(ArrayList<Elfo> lista, String nome){
        for(Elfo elfo : lista ){
            if(elfo.getNome().equals(nome)){
                return elfo;
            }
        }
        return null;
    }
    
    private Elfo buscarMap(HashMap<String, Elfo> map, String nome){
        return map.get(nome);
    }
}
