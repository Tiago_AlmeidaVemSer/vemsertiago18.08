import java.util.*;

public class Inventario extends Object{    
    private ArrayList<Item> itens;    
    
    public Inventario(int quantidadeItens) {
        this.itens = new ArrayList<>(quantidadeItens);
    }
       
    public ArrayList<Item> getItens(){
        return this.itens;
    }
    
    public void adicionar(Item item) {          
       this.itens.add(item);        
    }
    
    public Item obter(int posicao){
        if(posicao >= this.itens.size()){
            return null;
        }    
        return this.itens.get(posicao);
    }
    
    public void remover(int posicao) {        
            this.itens.remove(posicao);       
    }
    
    public void remover(Item item){
        this.itens.remove(item);
    }
    
    public String getDescricoesItens(){
        StringBuilder descricoes = new StringBuilder();
        int posicao = 0;
        for (Item item : this.itens) {
            posicao++;
            if (item != null) {
                String descricao = item.getDescricao();
                descricoes.append(descricao);
                boolean deveColocarVirgula = posicao < this.itens.size();
                if(deveColocarVirgula) {
                    descricoes.append(",");
                }
            }
        }        
        return descricoes.toString();
    }
    
    public Item maiorQtd(){
        int indice = 0, maiorQuantidadeParcial = 0;
        for (int i = 0; i < this.itens.size(); i++) {
            if( this.itens.get(i) != null ) {
                //int qtdAtual = this.obter(i).getQuantidade();
                int qtdAtual = this.itens.get(i).getQuantidade();
                if (qtdAtual > maiorQuantidadeParcial) {
                    maiorQuantidadeParcial = qtdAtual;
                    indice = i;
                }
            }
        }        
        //Logica de comparacao ? se verdadeiro : se falso;
        return this.itens.size() > 0 ? this.obter(indice) : null;
    }
        
    public Item buscar(String descricao){
       for ( Item item : this.itens ) {
            if ( item.getDescricao().equals(descricao) ) {
                return item;
           }           
       }
       return null;
    }
    
    public ArrayList<Item> inverter(){
        ArrayList<Item> invertidos = new ArrayList<>(this.itens.size());        
        for (int i = this.itens.size() - 1; i >= 0; i--) {            
            invertidos.add(this.itens.get(i));
        }
        return invertidos;
    }
    
    public void ordenarItens(){
        this.ordenarItens(TipoOrdenacao.ASC);
    }
    
    public void ordenarItens(TipoOrdenacao ordenacao){
        for ( int i = 0; i < this.itens.size(); i++) {
            for ( int j = 0; j < this.itens.size() - 1; j++ ) {
                Item atual = this.itens.get(j);
                Item proximo = this.itens.get(j + 1);
                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ?
                    atual.getQuantidade() > proximo.getQuantidade() :
                    atual.getQuantidade() < proximo.getQuantidade();
                if( deveTrocar ){
                    Item itemTrocado = atual;
                    this.itens.set(j, proximo);
                    this.itens.set(j + 1, itemTrocado);
                }
            }
        }
    }
}
