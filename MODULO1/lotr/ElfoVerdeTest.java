import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest {
    
    @Test
    public void elfoVerdeGanha2XpPorFlecha(){
        ElfoVerde elfoVerde = new ElfoVerde("Green");
        Dwarf dwarf = new Dwarf("Thurin");
        elfoVerde.atirarFlecha(dwarf);
        assertEquals(2,elfoVerde.getExperiencia());
    }
    
    @Test
    public void elfoVerdeAdicionaItemComDecricaoValida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        celebron.ganharItem(arcoDeVidro);
        Inventario inventario = celebron.getInventario();
        assertEquals( new Item(1, "Arco"), inventario.obter(0) );
        assertEquals( new Item(2, "Flecha"), inventario.obter(1) );
        assertEquals( arcoDeVidro, inventario.obter(2) );
    }
    
    @Test
    public void elfoVerdeAdicionaItemComDescricaoInvalida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item lanca = new Item(1, "lanca");
        celebron.ganharItem(lanca);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(1,"Arco"), inventario.obter(0));
        assertEquals(new Item(2,"Flecha"), inventario.obter(1));
        assertNull(inventario.buscar("lanca"));
    }
    
    @Test
    public void elfoVerdePerderItemComDescricaoValida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        celebron.ganharItem(arcoDeVidro);
        
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(1,"Arco"), inventario.obter(0));
        assertEquals(new Item(2,"Flecha"), inventario.obter(1));
        assertEquals(arcoDeVidro, inventario.obter(2));
        
        celebron.perderItem(arcoDeVidro);
        assertNull(inventario.buscar("Arco de Vidro"));
    }
    
    @Test
    public void elfoVerdePerdeItemComDecricaoInvalida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arco = new Item(1, "Arco");
        celebron.perderItem(arco);
        Inventario inventario = celebron.getInventario();
        assertEquals( new Item(1, "Arco"), inventario.obter(0) );
        assertEquals( new Item(2, "Flecha"), inventario.obter(1) );
    }
}
