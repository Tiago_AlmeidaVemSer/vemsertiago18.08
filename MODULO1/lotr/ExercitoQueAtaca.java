import java.util.*;

public class ExercitoQueAtaca extends ExercitoElfos{
    private Estrategia estrategia;
    
    public ExercitoQueAtaca(Estrategia estrategia){
        this.estrategia = estrategia;
    }
    
    public void trocarEstrategia(Estrategia estrategia){
        this.estrategia = estrategia;
    }
    
    public void atacar(ArrayList<Dwarf> dwarfs){
        ArrayList<Elfo> ordem = this.estrategia.getOrdemDeAtaque(this.getElfos());
        for (Elfo elfo : ordem){
            for (Dwarf dwarf : dwarfs){
                elfo.atirarFlecha(dwarf);
            }
        }
    }
}
