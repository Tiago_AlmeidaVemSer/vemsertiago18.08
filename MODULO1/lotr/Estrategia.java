import java.util.*;

@FunctionalInterface
public interface Estrategia {
    
    ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> ataques);
    
}
